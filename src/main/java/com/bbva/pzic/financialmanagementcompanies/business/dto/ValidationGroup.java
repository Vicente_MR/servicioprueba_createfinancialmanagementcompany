package com.bbva.pzic.financialmanagementcompanies.business.dto;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
public interface ValidationGroup {

    interface CreateSubscriptionRequest {
    }

    interface ModifySubscriptionRequest {
    }

    interface CreateReviewerSubscriptionRequest {
    }

    interface CreateFinancialManagementCompaniesRelatedContracts {
    }

    interface CreateFinancialManagementCompaniesBusinessManager {
    }

    interface CreateFinancialManagementCompany {
    }

    interface CreateFinancialManagementCompaniesAuthorizedBusinessManager {
    }

    interface SendEmailOtpNewUserSimpleWithPasswordOnce {
    }

    interface SendEmailOtpReactivationUserSimpleWithPasswordOnce {
    }

    interface SendEmailOtpGetBusinessManagerData {
    }

    interface SendEmailOtpDeleteUser {
    }

    interface SendEmailOtpDeleteGroup {
    }

    interface SendEmailOtpFinancialManagementCompaniesBusinessManager {
    }

    interface GetFMCAuthorizedBusinessManagerProfiledService {
    }

    interface ValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility {
    }

    interface ListFMCAuthorizedBusinessManagersProfiledServicesContracts {
    }
}
