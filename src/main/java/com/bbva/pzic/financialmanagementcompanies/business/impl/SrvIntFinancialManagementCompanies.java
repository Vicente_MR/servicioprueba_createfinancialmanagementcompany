package com.bbva.pzic.financialmanagementcompanies.business.impl;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.financialmanagementcompanies.business.ISrvIntFinancialManagementCompanies;
import com.bbva.pzic.financialmanagementcompanies.business.dto.*;
import com.bbva.pzic.financialmanagementcompanies.dao.IFinancialManagementCompaniesDAO;
import com.bbva.pzic.financialmanagementcompanies.dao.model.ksjo.ResponseData;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.*;
import com.bbva.pzic.financialmanagementcompanies.util.Errors;
import com.bbva.pzic.routine.validator.Validator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
@Service
public class SrvIntFinancialManagementCompanies implements ISrvIntFinancialManagementCompanies {

    private static final Log LOG = LogFactory.getLog(SrvIntFinancialManagementCompanies.class);
    @Autowired
    private IFinancialManagementCompaniesDAO financialManagementCompaniesDAO;
    @Autowired
    private Validator validator;

    /**
     * {@inheritDoc}
     */
    @Override
    public String createSubscriptionRequest(final SubscriptionRequest dtoInt) {
        LOG.info("... Invoking method SrvIntFinancialManagementCompanies.createSubscriptionRequest ...");
        LOG.info("... Validating createSubscriptionRequest input parameter ...");
        validator.validate(dtoInt, ValidationGroup.CreateSubscriptionRequest.class);
        return financialManagementCompaniesDAO.createSubscriptionRequest(dtoInt);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DTOIntSubscriptionRequests listSubscriptionRequests(final InputListSubscriptionRequests input) {
        LOG.info("... Invoking method SrvIntFinancialManagementCompanies.listSubscriptionRequests ...");
        return financialManagementCompaniesDAO.listSubscriptionRequests(input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SubscriptionRequest getSubscriptionRequest(final InputGetSubscriptionRequest input) {
        LOG.info("... Invoking method SrvIntFinancialManagementCompanies.getSubscriptionRequest ...");
        return financialManagementCompaniesDAO.getSubscriptionRequest(input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void modifySubscriptionRequest(final InputModifySubscriptionRequest input) {
        LOG.info("... Invoking method SrvIntFinancialManagementCompanies.modifySubscriptionRequest ...");
        LOG.info("... Validating modifySubscriptionRequest input parameter ...");
        validator.validate(input, ValidationGroup.ModifySubscriptionRequest.class);
        financialManagementCompaniesDAO.modifySubscriptionRequest(input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void modifyBusinessManagerSubscriptionRequest(
            final InputModifyBusinessManagerSubscriptionRequest input) {
        LOG.info("... Invoking method SrvIntFinancialManagementCompanies.modifyBusinessManagerSubscriptionRequest ...");
        financialManagementCompaniesDAO.modifyBusinessManagerSubscriptionRequest(input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String createReviewerSubscriptionRequest(
            final InputCreateReviewerSubscriptionRequest input) {
        LOG.info("... Invoking method SrvIntFinancialManagementCompanies.createReviewerSubscriptionRequest ...");
        validator.validate(input, ValidationGroup.CreateReviewerSubscriptionRequest.class);
        return financialManagementCompaniesDAO.createReviewerSubscriptionRequest(input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void createFinancialManagementCompaniesRelatedContracts(
            final InputCreateFinancialManagementCompaniesRelatedContracts input) {
        LOG.info("... Invoking method SrvIntFinancialManagementCompanies.createFinancialManagementCompaniesRelatedContracts ...");
        LOG.info("... Validating createFinancialManagementCompaniesRelatedContracts input parameter ...");
        validator.validate(input, ValidationGroup.CreateFinancialManagementCompaniesRelatedContracts.class);
        financialManagementCompaniesDAO.createFinancialManagementCompaniesRelatedContracts(input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String createFinancialManagementCompaniesBusinessManager(final DTOIntBusinessManager dtoInt) {
        LOG.info("... Invoking method SrvIntFinancialManagementCompanies.createFinancialManagementCompaniesBusinessManager ...");
        LOG.info("... Validating createFinancialManagementCompaniesBusinessManager input parameter ...");
        validator.validate(dtoInt, ValidationGroup.CreateFinancialManagementCompaniesBusinessManager.class);
        return financialManagementCompaniesDAO.createFinancialManagementCompaniesBusinessManager(dtoInt);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CreateAuthorizedBusinessManager createFinancialManagementCompaniesAuthorizedBusinessManager(
            final InputCreateAuthorizedBusinessManager input) {
        LOG.info("... Invoking method SrvIntFinancialManagementCompanies.createFinancialManagementCompaniesAuthorizedBusinessManager ...");
        LOG.info("... Validating createFinancialManagementCompaniesAuthorizedBusinessManager input parameter ...");
        validator.validate(input, ValidationGroup.CreateFinancialManagementCompaniesAuthorizedBusinessManager.class);
        return financialManagementCompaniesDAO.createFinancialManagementCompaniesAuthorizedBusinessManager(input);
    }

    @Override
    public void sendEmailOtpFinancialManagementCompaniesBusinessManager(final InputSendEmailOtpFinancialManagementCompaniesBusinessManager input) {
        LOG.info("[Envío de correo electrónico] - [se utiliza el backend B5]");
        validator.validate(input, ValidationGroup.SendEmailOtpFinancialManagementCompaniesBusinessManager.class);
        financialManagementCompaniesDAO.sendEmail(input);
    }

    @Override
    public ResponseData sendEmailOtpNewUserSimpleWithPasswordOnce(final InputRequestBackendRest input) {
        LOG.info("[Alta de usuario simple con password de un solo uso] - [se utiliza el servicio KSJO OP51]");
        validator.validate(input, ValidationGroup.SendEmailOtpNewUserSimpleWithPasswordOnce.class);

        return financialManagementCompaniesDAO.newUserSimpleWithPasswordOnce(input);
    }

    @Override
    public void sendEmailOtpReactivationUserSimpleWithPasswordOnce(final InputRequestBackendRest input) {
        LOG.info("[Reactivación simple de usuario con password de un solo uso] - [se utiliza el servicio KSJO OP52]");
        validator.validate(input, ValidationGroup.SendEmailOtpReactivationUserSimpleWithPasswordOnce.class);

        financialManagementCompaniesDAO.reactivateUserSimpleWithPasswordOnce(input);
    }

    @Override
    public InputSendEmailOtpFinancialManagementCompaniesBusinessManager sendEmailOtpGetBusinessManagerData(final InputSendEmailOtpFinancialManagementCompaniesBusinessManager input) {
        LOG.info("[Obtener datos de business-manager] - [se utiliza el TX KWHF]");
        validator.validate(input, ValidationGroup.SendEmailOtpGetBusinessManagerData.class);

        InputSendEmailOtpFinancialManagementCompaniesBusinessManager emails = financialManagementCompaniesDAO.getBusinessManagerData(input);

        if (emails == null) {
            LOG.info("Transacción KWHF no ha enviado datos");
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR);
        }

        return emails;
    }

    @Override
    public void sendEmailOtpDeleteUser(final InputRequestBackendRest input) {
        LOG.info("[Baja de Usuario] - [se utiliza el servicio KSJO op15]");
        validator.validate(input, ValidationGroup.SendEmailOtpDeleteUser.class);
        financialManagementCompaniesDAO.deleteUser(input);
    }

    @Override
    public void sendEmailOtpDeleteGroup(final InputRequestBackendRest input) {
        LOG.info("[Baja de Usuario en Grupo] - [se utiliza el servicio KSJO op16]");
        validator.validate(input, ValidationGroup.SendEmailOtpDeleteGroup.class);
        financialManagementCompaniesDAO.deleteGroup(input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProfiledService getFinancialManagementCompaniesAuthorizedBusinessManagerProfiledService(
            final InputGetFMCAuthorizedBusinessManagerProfiledService input) {
        LOG.info("... Invoking method SrvIntFinancialManagementCompanies.getFinancialManagementCompaniesAuthorizedBusinessManagerProfiledService ...");
        validator.validate(input, ValidationGroup.GetFMCAuthorizedBusinessManagerProfiledService.class);
        return financialManagementCompaniesDAO.getFinancialManagementCompaniesAuthorizedBusinessManagerProfiledService(input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ValidateOperationFeasibility validateFinancialManagementCompaniesAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility(
            final InputValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility input) {
        LOG.info("... Invoking method SrvIntFinancialManagementCompanies.validateFinancialManagementCompaniesAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility ...");
        LOG.info("... Validating validateFinancialManagementCompaniesAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility input parameter ...");
        validator.validate(input, ValidationGroup.ValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility.class);
        return financialManagementCompaniesDAO.validateFinancialManagementCompaniesAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility(input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DTOIntServiceContract listFinancialManagementCompaniesAuthorizedBusinessManagersProfiledServicesContracts(
            final InputListFMCAuthorizedBusinessManagersProfiledServicesContracts input) {
        LOG.info("... Invoking method SrvIntFinancialManagementCompanies.listFinancialManagementCompaniesAuthorizedBusinessManagersProfiledServicesContracts ...");
        validator.validate(input, ValidationGroup.ListFMCAuthorizedBusinessManagersProfiledServicesContracts.class);
        return financialManagementCompaniesDAO.listFinancialManagementCompaniesAuthorizedBusinessManagersProfiledServicesContracts(input);
    }

    /**
     * {@inheritDoc}
     * @return
     */
    public FinancialManagementCompanies createFinancialManagementCompany(final DTOIntFinancialManagementCompanies input) {
        LOG.info("... Invoking method SrvIntFinancialManagementCompanies.createFinancialManagementCompany ...");
        validator.validate(input, ValidationGroup.CreateFinancialManagementCompany.class);
        return financialManagementCompaniesDAO.createFinancialManagementCompany(input);
    }
}
