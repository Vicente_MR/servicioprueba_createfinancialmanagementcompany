package com.bbva.pzic.financialmanagementcompanies.business.dto;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
public class DTOIntProfile {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}