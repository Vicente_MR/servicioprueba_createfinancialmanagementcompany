package com.bbva.pzic.financialmanagementcompanies.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
public class DTOIntBusinessManagement {

    @Valid
    @NotNull(groups = ValidationGroup.CreateFinancialManagementCompany.class)
    private DTOIntManagementType managementType;

    public DTOIntManagementType getManagementType() {
        return managementType;
    }

    public void setManagementType(DTOIntManagementType managementType) {
        this.managementType = managementType;
    }
}