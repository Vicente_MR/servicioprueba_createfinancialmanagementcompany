package com.bbva.pzic.financialmanagementcompanies.business.dto;

public class DTOIntType {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
