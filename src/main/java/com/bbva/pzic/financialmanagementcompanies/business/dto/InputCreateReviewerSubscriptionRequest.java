package com.bbva.pzic.financialmanagementcompanies.business.dto;


import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.Reviewer;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created on 14/08/2018.
 *
 * @author Entelgy
 */
public class InputCreateReviewerSubscriptionRequest {


    @Size(groups = ValidationGroup.CreateReviewerSubscriptionRequest.class, max = 8)
    private String subscriptionRequestId;

    @Valid
    @NotNull(groups = ValidationGroup.CreateReviewerSubscriptionRequest.class)
    private Reviewer reviewer;

    public String getSubscriptionRequestId() {
        return subscriptionRequestId;
    }

    public void setSubscriptionRequestId(String subscriptionRequestId) {
        this.subscriptionRequestId = subscriptionRequestId;
    }

    public Reviewer getReviewer() {
        return reviewer;
    }

    public void setReviewer(Reviewer reviewer) {
        this.reviewer = reviewer;
    }
}
