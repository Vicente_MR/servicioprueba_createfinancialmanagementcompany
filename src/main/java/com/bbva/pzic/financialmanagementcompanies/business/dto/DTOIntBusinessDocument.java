package com.bbva.pzic.financialmanagementcompanies.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
public class DTOIntBusinessDocument {

    @Valid
    @NotNull(groups = ValidationGroup.CreateFinancialManagementCompany.class)
    private DTOIntBusinessDocumentType businessDocumentType;
    @NotNull(groups = ValidationGroup.CreateFinancialManagementCompany.class)
    private String documentNumber;
    private Date issueDate;
    private Date expirationDate;

    public DTOIntBusinessDocumentType getBusinessDocumentType() {
        return businessDocumentType;
    }

    public void setBusinessDocumentType(
            DTOIntBusinessDocumentType businessDocumentType) {
        this.businessDocumentType = businessDocumentType;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public Date getIssueDate() {
        if (issueDate == null) {
            return null;
        }
        return new Date(issueDate.getTime());
    }

    public void setIssueDate(Date issueDate) {
        if (issueDate == null) {
            this.issueDate = null;
        } else {
            this.issueDate = new Date(issueDate.getTime());
        }
    }

    public Date getExpirationDate() {
        if (expirationDate == null) {
            return null;
        }
        return new Date(expirationDate.getTime());
    }

    public void setExpirationDate(Date expirationDate) {
        if (expirationDate == null) {
            this.expirationDate = null;
        } else {
            this.expirationDate = new Date(expirationDate.getTime());
        }
    }
}