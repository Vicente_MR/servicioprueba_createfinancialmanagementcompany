package com.bbva.pzic.financialmanagementcompanies.business.dto;

import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.SubscriptionRequest;

import javax.validation.Valid;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
public class InputModifySubscriptionRequest {

    private String subscriptionRequestId;
    @Valid
    private SubscriptionRequest subscriptionRequest;

    public String getSubscriptionRequestId() {
        return subscriptionRequestId;
    }

    public void setSubscriptionRequestId(String subscriptionRequestId) {
        this.subscriptionRequestId = subscriptionRequestId;
    }

    public SubscriptionRequest getSubscriptionRequest() {
        return subscriptionRequest;
    }

    public void setSubscriptionRequest(
            SubscriptionRequest subscriptionRequest) {
        this.subscriptionRequest = subscriptionRequest;
    }
}
