package com.bbva.pzic.financialmanagementcompanies.business;

import com.bbva.pzic.financialmanagementcompanies.business.dto.*;
import com.bbva.pzic.financialmanagementcompanies.dao.model.ksjo.ResponseData;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.*;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
public interface ISrvIntFinancialManagementCompanies {

    /**
     * Creation of a subscription request, contains information related to a
     * company and the product that will be contracted.
     *
     * @param dtoInt dto with input fields to validate
     * @return identifier
     */
    String createSubscriptionRequest(SubscriptionRequest dtoInt);

    /**
     * List the generated subscription requests.
     *
     * @param input dto with input fields to validate
     * @return {@link DTOIntSubscriptionRequests}
     */
    DTOIntSubscriptionRequests listSubscriptionRequests(
            InputListSubscriptionRequests input);

    /**
     * Consult information related to a subscription request.
     *
     * @param input dto with input fields to validate
     * @return {@link SubscriptionRequest}
     */
    SubscriptionRequest getSubscriptionRequest(InputGetSubscriptionRequest input);

    /**
     * Modification of a subscription request.
     *
     * @param input dto with input fields to validate
     */
    void modifySubscriptionRequest(InputModifySubscriptionRequest input);

    /**
     * Modification of a business manager.
     *
     * @param input dto with input fields to validate
     */
    void modifyBusinessManagerSubscriptionRequest(
            InputModifyBusinessManagerSubscriptionRequest input);

    /**
     * Creation of a reviewer. Contains information related to the reviewer who
     * will manage the subscription request.
     *
     * @param input dto with input fields to validate
     * @return identifier
     */
    String createReviewerSubscriptionRequest(
            InputCreateReviewerSubscriptionRequest input);

    /**
     * Adding multiple related contracts in bulk operation.
     *
     * @param input dto with input fields to validate
     */
    void createFinancialManagementCompaniesRelatedContracts(
            InputCreateFinancialManagementCompaniesRelatedContracts input);

    /**
     * Service for creating a business manager. This information has no relation
     * with associated company, it is the personal information from the manager.
     *
     * @param dtoInt dto with input fields to validate
     * @return identifier
     */
    String createFinancialManagementCompaniesBusinessManager(DTOIntBusinessManager dtoInt);

    /**
     * Service for adding a new association between manager and a business.
     *
     * @param input dto with input fields to validate
     * @return {@link CreateAuthorizedBusinessManager}
     */
    CreateAuthorizedBusinessManager createFinancialManagementCompaniesAuthorizedBusinessManager(
            InputCreateAuthorizedBusinessManager input);

    void sendEmailOtpFinancialManagementCompaniesBusinessManager(
            InputSendEmailOtpFinancialManagementCompaniesBusinessManager input);

    ResponseData sendEmailOtpNewUserSimpleWithPasswordOnce(InputRequestBackendRest input);

    void sendEmailOtpReactivationUserSimpleWithPasswordOnce(InputRequestBackendRest input);

    InputSendEmailOtpFinancialManagementCompaniesBusinessManager sendEmailOtpGetBusinessManagerData(
            InputSendEmailOtpFinancialManagementCompaniesBusinessManager input);

    void sendEmailOtpDeleteUser(InputRequestBackendRest input);

    void sendEmailOtpDeleteGroup(InputRequestBackendRest input);

    /**
     * Service for retrieving a service of an authorized business manager.
     *
     * @param input dto with input fields to validate
     * @return {@link ProfiledService}
     */
    ProfiledService getFinancialManagementCompaniesAuthorizedBusinessManagerProfiledService(
            InputGetFMCAuthorizedBusinessManagerProfiledService input);

    /**
     * Validating that the profiled service, the account and the amount
     * associated with the service, comply with the necessary validations to be
     * performed.
     *
     * @param input dto with input fields to validate
     * @return {@link ValidateOperationFeasibility}
     */
    ValidateOperationFeasibility validateFinancialManagementCompaniesAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility(
            InputValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility input);

    /**
     * Service for retrieving a list of contracts that an authorized business
     * manager has profiled for a certain service.
     *
     * @param input dto with input fields to validate
     * @return {@link ServiceContract}
     */
    DTOIntServiceContract listFinancialManagementCompaniesAuthorizedBusinessManagersProfiledServicesContracts(
            InputListFMCAuthorizedBusinessManagersProfiledServicesContracts input);

    /**
     *
     * @return
     */

    FinancialManagementCompanies createFinancialManagementCompany(DTOIntFinancialManagementCompanies input);
}
