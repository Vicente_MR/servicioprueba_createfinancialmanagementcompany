package com.bbva.pzic.financialmanagementcompanies.business.dto;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
public class DTOIntAmount {

    @NotNull(groups = ValidationGroup.ValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility.class)
    @Digits(integer = 15, fraction = 2, groups = ValidationGroup.ValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility.class)
    private BigDecimal amount;

    @NotNull(groups = ValidationGroup.ValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility.class)
    @Size(max = 3, groups = ValidationGroup.ValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility.class)
    private String currency;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
