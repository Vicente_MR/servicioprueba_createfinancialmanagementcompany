package com.bbva.pzic.financialmanagementcompanies.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.Size;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
public class InputValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility {

    @Size(max = 8, groups = ValidationGroup.ValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility.class)
    private String financialManagementCompanyId;

    @Size(max = 8, groups = ValidationGroup.ValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility.class)
    private String authorizedBusinessManagerId;

    @Size(max = 4, groups = ValidationGroup.ValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility.class)
    private String profiledServiceId;

    @Valid
    private DTOIntValidateOperationFeasibility validateOperationFeasibility;

    public String getFinancialManagementCompanyId() {
        return financialManagementCompanyId;
    }

    public void setFinancialManagementCompanyId(
            String financialManagementCompanyId) {
        this.financialManagementCompanyId = financialManagementCompanyId;
    }

    public String getAuthorizedBusinessManagerId() {
        return authorizedBusinessManagerId;
    }

    public void setAuthorizedBusinessManagerId(
            String authorizedBusinessManagerId) {
        this.authorizedBusinessManagerId = authorizedBusinessManagerId;
    }

    public String getProfiledServiceId() {
        return profiledServiceId;
    }

    public void setProfiledServiceId(String profiledServiceId) {
        this.profiledServiceId = profiledServiceId;
    }

    public DTOIntValidateOperationFeasibility getValidateOperationFeasibility() {
        return validateOperationFeasibility;
    }

    public void setValidateOperationFeasibility(
            DTOIntValidateOperationFeasibility validateOperationFeasibility) {
        this.validateOperationFeasibility = validateOperationFeasibility;
    }
}
