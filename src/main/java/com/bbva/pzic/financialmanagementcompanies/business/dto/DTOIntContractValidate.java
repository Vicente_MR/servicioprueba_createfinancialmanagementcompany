package com.bbva.pzic.financialmanagementcompanies.business.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
public class DTOIntContractValidate {

    @NotNull(groups = ValidationGroup.ValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility.class)
    @Size(max = 20, groups = ValidationGroup.ValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility.class)
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
