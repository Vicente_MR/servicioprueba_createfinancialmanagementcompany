package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputCreateAuthorizedBusinessManager;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.CreateAuthorizedBusinessManager;

/**
 * Created on 18/09/2018.
 *
 * @author Entelgy
 */
public interface ICreateFinancialManagementCompaniesAuthorizedBusinessManagerMapper {

    InputCreateAuthorizedBusinessManager mapIn(
            String financialManagementCompanyId, CreateAuthorizedBusinessManager createAuthorizedBusinessManager);

    ServiceResponse<CreateAuthorizedBusinessManager> mapOut(CreateAuthorizedBusinessManager data);
}
