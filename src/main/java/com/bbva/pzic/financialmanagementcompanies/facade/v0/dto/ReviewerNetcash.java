package com.bbva.pzic.financialmanagementcompanies.facade.v0.dto;

import javax.validation.Valid;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "reviewerNetcash", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlType(name = "reviewerNetcash", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ReviewerNetcash implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Indicates the code that identifies the bank reviewer.
     */
    private String businessAgentId;

    private List<ContactDetail> contactDetails;
    /**
     * Reviewer role.
     */
    @Valid
    private ReviewerType reviewerType;
    /**
     * Management unit of the branch. A unit management is conformed by a group
     * of executives who attends a specific operations in a branch. A branch can
     * have several units management. Example:"000901".
     */
    private String unitManagement;
    /**
     * Bank associated with the business agent.
     */
    private Bank bank;
    /**
     * Agent profile.
     */
    private Profile profile;
    /**
     * Identifier associated to the classification of the business agent.
     */
    private String professionPosition;
    /**
     * BBVA identifier. This identifier is used for HR and authentication
     * purposes.
     */
    private String registrationIdentifier;

    public ReviewerType getReviewerType() {
        return reviewerType;
    }

    public void setReviewerType(ReviewerType reviewerType) {
        this.reviewerType = reviewerType;
    }

    public String getUnitManagement() {
        return unitManagement;
    }

    public void setUnitManagement(String unitManagement) {
        this.unitManagement = unitManagement;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public String getProfessionPosition() {
        return professionPosition;
    }

    public void setProfessionPosition(String professionPosition) {
        this.professionPosition = professionPosition;
    }

    public String getRegistrationIdentifier() {
        return registrationIdentifier;
    }

    public void setRegistrationIdentifier(String registrationIdentifier) {
        this.registrationIdentifier = registrationIdentifier;
    }

    public String getBusinessAgentId() {
        return businessAgentId;
    }

    public void setBusinessAgentId(String businessAgentId) {
        this.businessAgentId = businessAgentId;
    }

    public List<ContactDetail> getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(List<ContactDetail> contactDetails) {
        this.contactDetails = contactDetails;
    }

}
