package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputGetFMCAuthorizedBusinessManagerProfiledService;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.ProfiledService;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.IGetFMCAuthorizedBusinessManagerProfiledServiceMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
@Mapper
public class GetFMCAuthorizedBusinessManagerProfiledServiceMapper
        implements IGetFMCAuthorizedBusinessManagerProfiledServiceMapper {

    private static final Log LOG = LogFactory.getLog(GetFMCAuthorizedBusinessManagerProfiledServiceMapper.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public InputGetFMCAuthorizedBusinessManagerProfiledService mapIn(
            final String financialManagementCompanyId,
            final String authorizedBusinessManagerId,
            final String profiledServiceId) {
        LOG.info("... called method GetFMCAuthorizedBusinessManagerProfiledServiceMapper.mapIn ...");
        InputGetFMCAuthorizedBusinessManagerProfiledService input = new InputGetFMCAuthorizedBusinessManagerProfiledService();
        input.setFinancialManagementCompanyId(financialManagementCompanyId);
        input.setAuthorizedBusinessManagerId(authorizedBusinessManagerId);
        input.setProfiledServiceId(profiledServiceId);
        return input;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public ServiceResponse<ProfiledService> mapOut(
            final ProfiledService profiledService) {
        LOG.info("... called method GetFMCAuthorizedBusinessManagerProfiledServiceMapper.mapOut ...");
        if (profiledService == null) {
            return null;
        }
        return ServiceResponse.data(profiledService).pagination(null).build();
    }
}
