package com.bbva.pzic.financialmanagementcompanies.facade.v0.dto;

import com.bbva.pzic.financialmanagementcompanies.business.dto.ValidationGroup;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "businessDocumentType", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlType(name = "businessDocumentType", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class BusinessDocumentType implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Business document type.
     */
    @NotNull(groups = ValidationGroup.CreateSubscriptionRequest.class)
    private String id;
    /**
     * Document type name.
     */
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
