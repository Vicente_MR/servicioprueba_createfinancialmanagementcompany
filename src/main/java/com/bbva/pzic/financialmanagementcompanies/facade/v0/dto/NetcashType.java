package com.bbva.pzic.financialmanagementcompanies.facade.v0.dto;

import javax.validation.Valid;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 21/09/2018.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "netcashType", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlType(name = "netcashType", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class NetcashType implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Product code.
     */
    private String id;
    /**
     * Product description.
     */
    private String name;
    /**
     * Netcash product version information. This version is used to see the
     * configuration level that will apply for users.
     */
    @Valid
    private Version version;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Version getVersion() {
        return version;
    }

    public void setVersion(Version version) {
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public  void setName(String name) {
        this.name = name;
    }
}
