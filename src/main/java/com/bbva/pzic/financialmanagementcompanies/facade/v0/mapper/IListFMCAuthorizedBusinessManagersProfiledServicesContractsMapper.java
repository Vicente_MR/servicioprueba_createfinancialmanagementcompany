package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.Pagination;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputListFMCAuthorizedBusinessManagersProfiledServicesContracts;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.ServiceContract;

import java.util.List;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
public interface IListFMCAuthorizedBusinessManagersProfiledServicesContractsMapper {

    InputListFMCAuthorizedBusinessManagersProfiledServicesContracts mapIn(
            String financialManagementCompanyId,
            String authorizedBusinessManagerId, String profiledServiceId,
            String paginationKey, Integer pageSize);

    ServiceResponse<List<ServiceContract>> mapOut(List<ServiceContract> serviceContracts, Pagination pagination);
}
