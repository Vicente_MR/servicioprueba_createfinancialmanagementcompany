package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.financialmanagementcompanies.business.dto.*;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.*;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.ICreateFinancialManagementCompanyMapper;
import com.bbva.pzic.financialmanagementcompanies.util.Enums;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class CreateFinancialManagementCompanyMapper implements ICreateFinancialManagementCompanyMapper {

    private static final Log LOG = LogFactory.getLog(CreateFinancialManagementCompanyMapper.class);

    @Autowired
    private Translator translator;

    @Override
    public DTOIntFinancialManagementCompanies mapIn(final FinancialManagementCompanies financialManagementCompanies) {
        LOG.info("... called method CreateFinancialManagementCompanyMapper.mapIn ...");
        DTOIntFinancialManagementCompanies dtoInt = new DTOIntFinancialManagementCompanies();
        dtoInt.setBusiness(mapInBusiness(financialManagementCompanies.getBusiness()));
        dtoInt.setNetcashType(mapInNetcashType(financialManagementCompanies.getNetcashType()));
        dtoInt.setContract(mapInContract(financialManagementCompanies.getContract()));
        dtoInt.setProduct(mapInProduct(financialManagementCompanies.getProduct()));
        dtoInt.setRelationType(mapInRelationType(financialManagementCompanies.getRelationType()));
        dtoInt.setReviewers(mapInReviewers(financialManagementCompanies.getReviewers()));
        return dtoInt;
    }

    private List<DTOIntReviewerNetcash> mapInReviewers(final List<ReviewerNetcash> reviewers) {
        if (CollectionUtils.isEmpty(reviewers)){
            return null;
        }
        return reviewers.stream().filter(Objects::nonNull).map(this::mapInReviewer).collect(Collectors.toList());
    }

    private DTOIntReviewerNetcash mapInReviewer(final ReviewerNetcash reviewerNetcash) {
        if(reviewerNetcash == null){
            return null;
        }
        DTOIntReviewerNetcash dtoInt = new DTOIntReviewerNetcash();
        dtoInt.setBusinessAgentId(reviewerNetcash.getBusinessAgentId());
        dtoInt.setContactDetails(mapInContactDetails(reviewerNetcash.getContactDetails()));
        dtoInt.setReviewerType(mapInReviewerType(reviewerNetcash.getReviewerType()));
        dtoInt.setUnitManagement(reviewerNetcash.getUnitManagement());
        dtoInt.setBank(mapInBank(reviewerNetcash.getBank()));
        dtoInt.setProfile(mapInProfile(reviewerNetcash.getProfile()));
        dtoInt.setProfessionPosition(reviewerNetcash.getProfessionPosition());
        dtoInt.setRegistrationIdentifier(reviewerNetcash.getRegistrationIdentifier());
        return dtoInt;
    }

    private DTOIntProfile mapInProfile(final Profile profile) {
        if(profile == null){
            return null;
        }
        DTOIntProfile dtoInt = new DTOIntProfile();
        dtoInt.setId(profile.getId());
        return dtoInt;
    }

    private DTOIntBank mapInBank(final Bank bank) {
        if(bank == null){
            return null;
        }
        DTOIntBank dtoInt = new DTOIntBank();
        dtoInt.setId(bank.getId());
        dtoInt.setBranch(mapInBranch(bank.getBranch()));
        return dtoInt;
    }

    private DTOIntBranch mapInBranch(final Branch branch) {
        if(branch == null){
            return null;
        }
        DTOIntBranch dtoInt = new DTOIntBranch();
        dtoInt.setId(branch.getId());
        return dtoInt;
    }

    private DTOIntReviewerType mapInReviewerType(final ReviewerType reviewerType) {
        if(reviewerType == null){
            return null;
        }
        DTOIntReviewerType dtoInt = new DTOIntReviewerType();
        dtoInt.setId(translator.translateFrontendEnumValueStrictly(Enums.ENUM_SUSCRIPTIONREQUEST_REVIEWER_ID, reviewerType.getId()));
        return dtoInt;
    }

    private List<DTOIntContactDetail> mapInContactDetails(final List<ContactDetail> contactDetails) {
        if(CollectionUtils.isEmpty(contactDetails)){
            return null;
        }
        return contactDetails.stream().filter(Objects::nonNull).map(this::mapInContactDetail).collect(Collectors.toList());
    }

    private DTOIntContactDetail mapInContactDetail(final ContactDetail contactDetail) {
        if(contactDetail == null){
            return null;
        }
        DTOIntContactDetail dtoInt = new DTOIntContactDetail();
        dtoInt.setContact(contactDetail.getContact());
        dtoInt.setContactType(translator.translateFrontendEnumValueStrictly(Enums.ENUM_CONTACTDETAILS_CONTACTTYPE_ID, contactDetail.getContactType()));
        return dtoInt;
    }

    private DTOIntRelationType mapInRelationType(final RelationType relationType) {
        if(relationType == null){
            return null;
        }
        DTOIntRelationType dtoInt = new DTOIntRelationType();
        dtoInt.setId(translator.translateFrontendEnumValueStrictly(Enums.ENUM_FINANCIALMANAGEMENTCOMPANY_RELATEDCONTRACTS_RELATIONTYPE, relationType.getId()));
        return dtoInt;
    }

    private DTOIntRelatedProduct mapInProduct(final RelatedProduct product) {
        if(product == null){
            return null;
        }
        DTOIntRelatedProduct dtoInt = new DTOIntRelatedProduct();
        dtoInt.setId(product.getId());
        dtoInt.setProductType(mapInProductType(product.getProductType()));
        return dtoInt;
    }

    private DTOIntProductType mapInProductType(final ProductType productType) {
        if(productType == null){
            return null;
        }
        DTOIntProductType dtoInt = new DTOIntProductType();
        dtoInt.setId(translator.translateFrontendEnumValueStrictly(Enums.ENUM_FINANCIALMANAGEMENTCOMPANY_PRODUCTTYPE_ID, productType.getId()));
        return dtoInt;
    }

    private DTOIntContract mapInContract(final Contract contract) {
        if(contract == null){
            return null;
        }
        DTOIntContract dtoInt = new DTOIntContract();
        dtoInt.setId(contract.getId());
        return dtoInt;
    }

    private DTOIntNetcashType mapInNetcashType(final NetcashType netcashType) {
        if(netcashType == null){
            return null;
        }
        DTOIntNetcashType dtoInt = new DTOIntNetcashType();
        dtoInt.setId(translator.translateFrontendEnumValueStrictly(Enums.ENUM_SUSCRIPTIONREQUEST_PRODUCT_ID, netcashType.getId()));
        dtoInt.setVersion(mapInVersion(netcashType.getVersion()));
        return dtoInt;
    }

    private DTOIntVersionProduct mapInVersion(final Version version) {
        if(version == null){
            return null;
        }
        DTOIntVersionProduct dtoInt = new DTOIntVersionProduct();
        dtoInt.setId(translator.translateFrontendEnumValueStrictly(Enums.ENUM_FINANCIALMANAGEMENTCOMPANY_VERSION_ID, version.getId()));
        return dtoInt;
    }

    private DTOIntBusiness mapInBusiness(final Business business) {
        if (business == null){
            return null;
        }
        DTOIntBusiness dtoInt = new DTOIntBusiness();
        dtoInt.setBusinessDocuments(mapInBusinessDocuments(business.getBusinessDocuments()));
        dtoInt.setBusinessManagement(mapInBusinessManagement(business.getBusinessManagement()));
        dtoInt.setLimitAmount(mapInBusinessLimitAmount(business.getLimitAmount()));
        return dtoInt;
    }

    private DTOIntLimitAmount mapInBusinessLimitAmount(final LimitAmount limitAmount) {
        if(limitAmount == null){
            return null;
        }
        DTOIntLimitAmount dtoInt = new DTOIntLimitAmount();
        dtoInt.setAmount(limitAmount.getAmount());
        dtoInt.setCurrency(limitAmount.getCurrency());

        return dtoInt;
    }

    private DTOIntBusinessManagement mapInBusinessManagement(final BusinessManagement businessManagement) {
        if(businessManagement == null) {
            return null;
        }
        DTOIntBusinessManagement dtoInt = new DTOIntBusinessManagement();
        dtoInt.setManagementType(mapInBusinessManagementType(businessManagement.getManagementType()));

        return dtoInt;
    }

    private DTOIntManagementType mapInBusinessManagementType(ManagementType managementType) {
        if(managementType == null){
            return null;
        }
        DTOIntManagementType dtoInt = new DTOIntManagementType();
        dtoInt.setId(translator.translateFrontendEnumValueStrictly(Enums.ENUM_FINANCIALMANAGEMENTCOMPANY_BUSINESSMANAGEMENT_MANAGEMENTTYPE_ID, managementType.getId()));
        return dtoInt;
    }

    private List<DTOIntBusinessDocument> mapInBusinessDocuments(final List<BusinessDocument> businessDocuments) {
        if (CollectionUtils.isEmpty(businessDocuments)){
            return null;
        }
        return businessDocuments.stream().filter(Objects::nonNull).map(this::mapInBusinessDocument).collect(Collectors.toList());
    }

    private DTOIntBusinessDocument mapInBusinessDocument(final BusinessDocument businessDocument) {
        if (businessDocument == null){
            return null;
        }
        DTOIntBusinessDocument dtoInt = new DTOIntBusinessDocument();
        dtoInt.setBusinessDocumentType(mapInBusinessDocumentType(businessDocument.getBusinessDocumentType()));
        dtoInt.setDocumentNumber(businessDocument.getDocumentNumber());
        dtoInt.setIssueDate(businessDocument.getIssueDate());
        dtoInt.setExpirationDate(businessDocument.getExpirationDate());
        return dtoInt;
    }

    private DTOIntBusinessDocumentType mapInBusinessDocumentType(final BusinessDocumentType businessDocumentType) {
        if (businessDocumentType == null){
            return null;
        }
        DTOIntBusinessDocumentType dtoInt = new DTOIntBusinessDocumentType();
        dtoInt.setId(translator.translateFrontendEnumValueStrictly(Enums.ENUM_DOCUMENTTYPE_ID, businessDocumentType.getId()));
        return dtoInt;
    }

    @Override
    public ServiceResponse<FinancialManagementCompanies> mapOut(final FinancialManagementCompanies input) {
        if(input == null){
            return null;
        }
        return ServiceResponse.data(input).build();
    }
}
