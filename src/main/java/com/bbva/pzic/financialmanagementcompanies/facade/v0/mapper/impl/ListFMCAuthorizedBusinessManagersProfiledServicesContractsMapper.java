package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.Pagination;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputListFMCAuthorizedBusinessManagersProfiledServicesContracts;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.ServiceContract;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.IListFMCAuthorizedBusinessManagersProfiledServicesContractsMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
@Mapper
public class ListFMCAuthorizedBusinessManagersProfiledServicesContractsMapper
        implements IListFMCAuthorizedBusinessManagersProfiledServicesContractsMapper {

    private static final Log LOG = LogFactory.getLog(ListFMCAuthorizedBusinessManagersProfiledServicesContractsMapper.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public InputListFMCAuthorizedBusinessManagersProfiledServicesContracts mapIn(
            final String financialManagementCompanyId,
            final String authorizedBusinessManagerId,
            final String profiledServiceId,
            final String paginationKey,
            final Integer pageSize) {
        LOG.info("... called method ListFMCAuthorizedBusinessManagersProfiledServicesContractsMapper.mapIn ...");
        InputListFMCAuthorizedBusinessManagersProfiledServicesContracts input = new InputListFMCAuthorizedBusinessManagersProfiledServicesContracts();
        input.setFinancialManagementCompanyId(financialManagementCompanyId);
        input.setAuthorizedBusinessManagerId(authorizedBusinessManagerId);
        input.setProfiledServiceId(profiledServiceId);
        input.setPaginationKey(paginationKey);
        input.setPageSize(pageSize);
        return input;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public ServiceResponse<List<ServiceContract>> mapOut(
            final List<ServiceContract> serviceContracts, final Pagination pagination) {
        LOG.info("... called method ListFMCAuthorizedBusinessManagersProfiledServicesContractsMapper.mapOut ...");
        if (serviceContracts.isEmpty()) {
            return null;
        }
        return ServiceResponse.data(serviceContracts).pagination(pagination).build();
    }
}
