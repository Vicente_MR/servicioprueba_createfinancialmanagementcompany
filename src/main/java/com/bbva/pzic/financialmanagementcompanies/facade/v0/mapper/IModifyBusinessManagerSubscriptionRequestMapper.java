package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputModifyBusinessManagerSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.BusinessManagerSubscriptionRequest;

/**
 * Created on 14/08/2018.
 *
 * @author Entelgy
 */
public interface IModifyBusinessManagerSubscriptionRequestMapper {

    InputModifyBusinessManagerSubscriptionRequest mapIn(
            String subscriptionRequestId,
            String businessManagerId,
            BusinessManagerSubscriptionRequest businessManagerSubscriptionRequest);
}
