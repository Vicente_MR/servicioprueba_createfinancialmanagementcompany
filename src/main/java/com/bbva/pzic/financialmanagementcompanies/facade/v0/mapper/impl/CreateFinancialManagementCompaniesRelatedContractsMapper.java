package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputCreateFinancialManagementCompaniesRelatedContracts;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.RelatedContract;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.ICreateFinancialManagementCompaniesRelatedContractsMapper;
import com.bbva.pzic.financialmanagementcompanies.util.Errors;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.EnumMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.Mapper;
import com.bbva.pzic.financialmanagementcompanies.util.orika.impl.ConfigurableMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created on 13/09/2018.
 *
 * @author Entelgy
 */
@Mapper
public class CreateFinancialManagementCompaniesRelatedContractsMapper extends ConfigurableMapper implements ICreateFinancialManagementCompaniesRelatedContractsMapper {

    private static final Log LOG = LogFactory.getLog(CreateFinancialManagementCompaniesRelatedContractsMapper.class);

    @Autowired
    private EnumMapper enumMapper;

    /**
     * {@inheritDoc}
     */
    @Override
    public InputCreateFinancialManagementCompaniesRelatedContracts mapIn(final String financialManagementCompanyId, final List<RelatedContract> relatedContract) {
        LOG.info("... called method CreateFinancialManagementCompaniesRelatedContractsMapper.mapIn ...");
        InputCreateFinancialManagementCompaniesRelatedContracts input = new InputCreateFinancialManagementCompaniesRelatedContracts();
        if (relatedContract.get(0).getProduct() == null &&
                relatedContract.get(0).getContract() == null &&
                relatedContract.get(0).getRelationType() == null)
            return input;

        if (relatedContract.get(0) == null ||
                relatedContract.get(0).getContract().getId() == null || relatedContract.get(0).getProduct().getId() == null ||
                relatedContract.get(0).getProduct().getProductType().getId() == null ||
                relatedContract.get(0).getRelationType().getId() == null)
            throw new BusinessServiceException(Errors.MANDATORY_PARAMETERS_MISSING);

        input.setFinancialManagementCompanyId(financialManagementCompanyId);

        for (RelatedContract rel : relatedContract) {
            rel.getProduct().getProductType().setId(enumMapper.getBackendValue("productType.id", rel.getProduct().getProductType().getId()));
            rel.getRelationType().setId(enumMapper.getBackendValue("relationType.id", rel.getRelationType().getId()));
        }
        input.setRelatedContract(relatedContract);

        return input;
    }

}
