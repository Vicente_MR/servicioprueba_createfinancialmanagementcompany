package com.bbva.pzic.financialmanagementcompanies.facade.v0.dto;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import com.bbva.pzic.financialmanagementcompanies.business.dto.ValidationGroup;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "contactDetail", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlType(name = "contactDetail", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ContactDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Contact information type identifier. DISCLAIMER: A business manager can
     * only have one LOGIN_EMAIL.
     */
    @NotNull(groups = {
            ValidationGroup.CreateSubscriptionRequest.class,
            ValidationGroup.CreateReviewerSubscriptionRequest.class
    })
    @Size(groups = ValidationGroup.CreateReviewerSubscriptionRequest.class, max = 20)
    private String contactType;
    /**
     * Contact information value.
     */
    @NotNull(groups = {
            ValidationGroup.CreateSubscriptionRequest.class,
            ValidationGroup.CreateReviewerSubscriptionRequest.class
    })
    @Size(groups = ValidationGroup.CreateReviewerSubscriptionRequest.class, max = 30)
    @DatoAuditable(omitir = true)
    private String contact;

    public String getContactType() {
        return contactType;
    }

    public void setContactType(String contactType) {
        this.contactType = contactType;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }
}
