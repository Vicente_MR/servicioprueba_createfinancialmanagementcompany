package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.ValidateOperationFeasibility;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
public interface IValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibilityMapper {

    InputValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility mapIn(
            String financialManagementCompanyId,
            String authorizedBusinessManagerId, String profiledServiceId,
            ValidateOperationFeasibility validateOperationFeasibility);

    ServiceResponse<ValidateOperationFeasibility> mapOut(
            ValidateOperationFeasibility validateOperationFeasibility);
}
