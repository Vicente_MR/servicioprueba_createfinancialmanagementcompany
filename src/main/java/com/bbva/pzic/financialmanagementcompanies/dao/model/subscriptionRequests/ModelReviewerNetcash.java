package com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests;

/**
 * Created on 18/09/2018.
 *
 * @author Entelgy
 */
public class ModelReviewerNetcash {

    private String businessAgentId;
    private ModelReviewerType reviewerType;
    private String unitManagement;
    private ModelBank bank;
    private ModelProfile profile;
    private String professionPosition;
    private String registrationIdentifier;

    public String getBusinessAgentId() {
        return businessAgentId;
    }

    public void setBusinessAgentId(String businessAgentId) {
        this.businessAgentId = businessAgentId;
    }

    public ModelReviewerType getReviewerType() {
        return reviewerType;
    }

    public void setReviewerType(ModelReviewerType reviewerType) {
        this.reviewerType = reviewerType;
    }

    public String getUnitManagement() {
        return unitManagement;
    }

    public void setUnitManagement(String unitManagement) {
        this.unitManagement = unitManagement;
    }

    public ModelBank getBank() {
        return bank;
    }

    public void setBank(ModelBank bank) {
        this.bank = bank;
    }

    public ModelProfile getProfile() {
        return profile;
    }

    public void setProfile(ModelProfile profile) {
        this.profile = profile;
    }

    public String getProfessionPosition() {
        return professionPosition;
    }

    public void setProfessionPosition(String professionPosition) {
        this.professionPosition = professionPosition;
    }

    public String getRegistrationIdentifier() {
        return registrationIdentifier;
    }

    public void setRegistrationIdentifier(String registrationIdentifier) {
        this.registrationIdentifier = registrationIdentifier;
    }
}