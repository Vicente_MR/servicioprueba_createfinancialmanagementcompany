package com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

/**
 * @author Entelgy
 */
public class ModelContactInformation {

    private String id;
    @DatoAuditable(omitir = true)
    private String contact;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }
}