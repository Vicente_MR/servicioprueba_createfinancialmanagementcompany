package com.bbva.pzic.financialmanagementcompanies.dao.rest.mock;

import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelReviewerSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelReviewerSubscriptionResponse;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.RestCreateReviewerSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mock.stubs.ResponseFinancialManagementCompaniesMock;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created on 26/09/2018.
 *
 * @author Entelgy
 */
@Primary
@Component
public class RestCreateReviewerSubscriptionRequestMock extends RestCreateReviewerSubscriptionRequest {

    @Override
    protected ModelReviewerSubscriptionResponse connect(String urlPropertyValue, Map<String, String> pathParams, ModelReviewerSubscriptionRequest entityPayload) {
        return ResponseFinancialManagementCompaniesMock.getInstance().buildModelReviewerSubscriptionResponse();
    }
}
