package com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests;

/**
 * Created on 18/09/2018.
 *
 * @author Entelgy
 */
public class ModelBank {

    private String id;
    private ModelBranch branch;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ModelBranch getBranch() {
        return branch;
    }

    public void setBranch(ModelBranch branch) {
        this.branch = branch;
    }
}