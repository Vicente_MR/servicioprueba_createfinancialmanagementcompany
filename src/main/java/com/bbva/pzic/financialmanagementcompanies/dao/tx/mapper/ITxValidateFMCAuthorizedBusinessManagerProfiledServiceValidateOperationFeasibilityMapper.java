package com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwlv.FormatoKNECLVE0;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwlv.FormatoKNECLVS0;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.ValidateOperationFeasibility;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
public interface ITxValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibilityMapper {

    FormatoKNECLVE0 mapIn(InputValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility dtoIn);

    ValidateOperationFeasibility mapOut(FormatoKNECLVS0 formatOutput);
}
