package com.bbva.pzic.financialmanagementcompanies.dao.rest.mock;

import com.bbva.pzic.financialmanagementcompanies.dao.model.ksjo.BodyDataRest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.ksjo.ResponseData;
import com.bbva.pzic.financialmanagementcompanies.dao.model.ksjo.mock.ResponseDataMockBuilder;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.RestNewUserSimpleWithPasswordOnce;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created on 26/09/2018.
 *
 * @author Entelgy
 */
@Primary
@Component
public class RestNewUserSimpleWithPasswordOnceMock extends RestNewUserSimpleWithPasswordOnce {

    private static final String ERROR_STATUS_CODE = "MDO_008";
    private static final String ERROR_STATUS_DESCRIPTION = "El usuario ya existe";

    private static final String ERROR_RESPONSE = "6666";
    private static final String EMPTY_RESPONSE = "6667";

    private ResponseDataMockBuilder responseDataMockBuilder;

    @Override
    public void init() {
        super.init();
        responseDataMockBuilder = new ResponseDataMockBuilder()
                .errorStatusCode(ERROR_STATUS_CODE)
                .errorStatusDescription(ERROR_STATUS_DESCRIPTION)
                .errorResponse(ERROR_RESPONSE)
                .emptyResponse(EMPTY_RESPONSE);
    }

    @Override
    public ResponseData connect(String urlPropertyValue, Map<String, String> pathParams, HashMap<String, String> params, Map<String, String> headers, BodyDataRest entityPayload) {
        ResponseData responseData = responseDataMockBuilder.build(entityPayload.getDataOperation().getAlias());
        evaluateResponse(responseData, 200);
        return responseData;
    }
}
