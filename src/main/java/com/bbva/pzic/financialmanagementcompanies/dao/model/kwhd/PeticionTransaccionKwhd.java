package com.bbva.pzic.financialmanagementcompanies.dao.model.kwhd;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>KWHD</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionKwhd</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionKwhd</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTCCT.KWHD.D1181107.TXT
 * KWHDALTA DE USUARIOS ASO - ONBOARDING  KN        KN2CKWHDPBDKNPO KNECBEHD            KWHD  NS0500CNNNNN    SSTN    C   SNNNSNNN  NN                2018-03-06P016788 2018-10-0212.45.18XP91621 2018-03-06-16.54.20.886606P016788 0001-01-010001-01-01
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.KNECBEHD.D1181107.TXT
 * KNECBEHD�ENTRADA ALTA USUARIOS ASO     �F�15�00370�01�00001�TIPUS1 �TIPO DE USUARIO 1   �A�001�0�O�        �
 * KNECBEHD�ENTRADA ALTA USUARIOS ASO     �F�15�00370�02�00002�TIPUS2 �TIPO DE USUARIO 2   �A�001�0�O�        �
 * KNECBEHD�ENTRADA ALTA USUARIOS ASO     �F�15�00370�03�00003�TIPUS3 �TIPO DE USUARIO 3   �A�001�0�O�        �
 * KNECBEHD�ENTRADA ALTA USUARIOS ASO     �F�15�00370�04�00004�CODEMP �CODIGO DE EMPRESA   �A�008�0�R�        �
 * KNECBEHD�ENTRADA ALTA USUARIOS ASO     �F�15�00370�05�00012�NOMUSU �NOMBRE DE USUARIO   �A�030�0�O�        �
 * KNECBEHD�ENTRADA ALTA USUARIOS ASO     �F�15�00370�06�00042�DOCID  �TIPO DE DOCUMENTO ID�A�001�0�R�        �
 * KNECBEHD�ENTRADA ALTA USUARIOS ASO     �F�15�00370�07�00043�NRODID �NUMERO DE DOCUMENTO �A�012�0�R�        �
 * KNECBEHD�ENTRADA ALTA USUARIOS ASO     �F�15�00370�08�00055�TIPCO0 �TIPO DE CONTACTO 0  �A�001�0�R�        �
 * KNECBEHD�ENTRADA ALTA USUARIOS ASO     �F�15�00370�09�00056�DACO0  �DATO DEL CONTACTO 0 �A�078�0�R�        �
 * KNECBEHD�ENTRADA ALTA USUARIOS ASO     �F�15�00370�10�00134�TIPCO1 �TIPO DE CONTACTO 1  �A�001�0�O�        �
 * KNECBEHD�ENTRADA ALTA USUARIOS ASO     �F�15�00370�11�00135�DACO1  �DATO DEL CONTACTO 1 �A�078�0�O�        �
 * KNECBEHD�ENTRADA ALTA USUARIOS ASO     �F�15�00370�12�00213�TIPCO2 �TIPO DE CONTACTO 2  �A�001�0�O�        �
 * KNECBEHD�ENTRADA ALTA USUARIOS ASO     �F�15�00370�13�00214�DACO2  �DATO DEL CONTACTO 2 �A�078�0�O�        �
 * KNECBEHD�ENTRADA ALTA USUARIOS ASO     �F�15�00370�14�00292�TIPCO3 �TIPO DE CONTACTO 3  �A�001�0�O�        �
 * KNECBEHD�ENTRADA ALTA USUARIOS ASO     �F�15�00370�15�00293�DACO3  �DATO DEL CONTACTO 3 �A�078�0�O�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.KNECBSHD.D1181107.TXT
 * KNECBSHD�SALIDA ALTA USUARIOS ASO      �X�01�00024�01�00001�USUID  �CODIGO ID DE USER   �A�024�0�S�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.KWHD.D1181107.TXT
 * KWHDKNECBSHDKNECBSHDKN2CKWHD1S0155S000                     P016788 2018-03-06-17.40.23.137134P016788 2018-03-06-17.41.52.281960
</pre></code>
 *
 * @see RespuestaTransaccionKwhd
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "KWHD",
	tipo = 1,
	subtipo = 1,
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionKwhd.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoKNECBEHD.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionKwhd implements MensajeMultiparte {

	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}

}