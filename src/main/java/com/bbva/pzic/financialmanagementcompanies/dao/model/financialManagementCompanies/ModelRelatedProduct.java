package com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompanies;

/**
 * Created on 16/12/2020.
 *
 * @author Entelgy
 */
public class ModelRelatedProduct {
    private String id;
    private ModelProductType productType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ModelProductType getProductType() {
        return productType;
    }

    public void setProductType(ModelProductType productType) {
        this.productType = productType;
    }
}
