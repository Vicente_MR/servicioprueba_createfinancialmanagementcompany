package com.bbva.pzic.financialmanagementcompanies.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwlv.FormatoKNECLVE0;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwlv.FormatoKNECLVS0;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwlv.PeticionTransaccionKwlv;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwlv.RespuestaTransaccionKwlv;
import com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper.ITxValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibilityMapper;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.ValidateOperationFeasibility;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.SingleOutputFormat;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
@Component("txValidateFinancialManagementCompaniesAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility")
public class TxValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility
        extends SingleOutputFormat<InputValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility, FormatoKNECLVE0, ValidateOperationFeasibility, FormatoKNECLVS0> {

    @Resource(name = "txValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibilityMapper")
    private ITxValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibilityMapper mapper;

    public TxValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility(@Qualifier("transaccionKwlv") InvocadorTransaccion<PeticionTransaccionKwlv, RespuestaTransaccionKwlv> transaction) {
        super(transaction, PeticionTransaccionKwlv::new, ValidateOperationFeasibility::new, FormatoKNECLVS0.class);
    }

    @Override
    protected FormatoKNECLVE0 mapInput(InputValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility inputValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility) {
        return mapper.mapIn(inputValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility);
    }

    @Override
    protected ValidateOperationFeasibility mapFirstOutputFormat(FormatoKNECLVS0 formatoKNECLVS0, InputValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility inputValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility, ValidateOperationFeasibility validateOperationFeasibility) {
        return mapper.mapOut(formatoKNECLVS0);
    }

}
