package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.business.dto.*;
import com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompanies.*;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestCreateFinancialManagementCompanyMapper;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.FinancialManagementCompanies;
import com.bbva.pzic.financialmanagementcompanies.util.Enums;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class RestCreateFinancialManagementCompanyMapper implements IRestCreateFinancialManagementCompanyMapper {
    private static final Log LOG = LogFactory.getLog(RestCreateFinancialManagementCompanyMapper.class);

    @Autowired
    private Translator translator;

    @Override
    public ModelFinancialManagementCompaniesRequest mapIn(final DTOIntFinancialManagementCompanies financialManagementCompanies) {
        LOG.info("... called method RestCreateFinancialManagementCompanyMapper.mapIn ...");
        ModelFinancialManagementCompaniesRequest dtoInt = new ModelFinancialManagementCompaniesRequest();
        dtoInt.setBusiness(mapInBusiness(financialManagementCompanies.getBusiness()));
        dtoInt.setNetcashType(mapInNetcashType(financialManagementCompanies.getNetcashType()));
        dtoInt.setContract(mapInContract(financialManagementCompanies.getContract()));
        dtoInt.setProduct(mapInProduct(financialManagementCompanies.getProduct()));
        dtoInt.setRelationType(mapInRelationType(financialManagementCompanies.getRelationType()));
        dtoInt.setReviewers(mapInReviewers(financialManagementCompanies.getReviewers()));
        return dtoInt;
    }

    private ModelNetcashType mapInNetcashType(final DTOIntNetcashType netcashType) {
        if(netcashType == null){
            return null;
        }
        ModelNetcashType dtoInt = new ModelNetcashType();
        dtoInt.setId(translator.translateFrontendEnumValueStrictly(Enums.ENUM_SUSCRIPTIONREQUEST_PRODUCT_ID, netcashType.getId()));
        dtoInt.setVersion(mapInVersion(netcashType.getVersion()));
        return dtoInt;
    }

    private ModelVersion mapInVersion(final DTOIntVersionProduct version) {
        if(version == null){
            return null;
        }
        ModelVersion dtoInt = new ModelVersion();
        dtoInt.setId(translator.translateFrontendEnumValueStrictly(Enums.ENUM_FINANCIALMANAGEMENTCOMPANY_VERSION_ID, version.getId()));
        return dtoInt;
    }

    private ModelContract mapInContract(final DTOIntContract contract) {
        if(contract == null){
            return null;
        }
        ModelContract dtoInt = new ModelContract();
        dtoInt.setId(contract.getId());
        return dtoInt;
    }

    private ModelRelatedProduct mapInProduct(final DTOIntRelatedProduct product) {
        if(product == null){
            return null;
        }
        ModelRelatedProduct dtoInt = new ModelRelatedProduct();
        dtoInt.setId(product.getId());
        dtoInt.setProductType(mapInProductType(product.getProductType()));
        return dtoInt;
    }

    private ModelProductType mapInProductType(final DTOIntProductType productType) {
        if(productType == null){
            return null;
        }
        ModelProductType dtoInt = new ModelProductType();
        dtoInt.setId(translator.translateFrontendEnumValueStrictly(Enums.ENUM_FINANCIALMANAGEMENTCOMPANY_PRODUCTTYPE_ID, productType.getId()));
        return dtoInt;
    }

    private ModelRelationType mapInRelationType(final DTOIntRelationType relationType) {
        if(relationType == null){
            return null;
        }
        ModelRelationType dtoInt = new ModelRelationType();
        dtoInt.setId(translator.translateFrontendEnumValueStrictly(Enums.ENUM_FINANCIALMANAGEMENTCOMPANY_RELATEDCONTRACTS_RELATIONTYPE, relationType.getId()));
        return dtoInt;
    }

    private List<ModelReviewers> mapInReviewers(final List<DTOIntReviewerNetcash> reviewers) {
        if(CollectionUtils.isEmpty(reviewers)){
            return null;
        }
        return reviewers.stream().filter(Objects::nonNull).map(this::mapInReviewer).collect(Collectors.toList());
    }

    private ModelReviewers mapInReviewer(final DTOIntReviewerNetcash reviewerNetcash) {
        ModelReviewers dtoInt = new ModelReviewers();
        dtoInt.setBusinessAgentId(reviewerNetcash.getBusinessAgentId());
        dtoInt.setContactDetails(mapInContacDetails(reviewerNetcash.getContactDetails()));
        dtoInt.setReviewerType(mapInReviewerType(reviewerNetcash.getReviewerType()));
        dtoInt.setUnitManagement(reviewerNetcash.getUnitManagement());
        dtoInt.setBank(mapInBank(reviewerNetcash.getBank()));
        dtoInt.setProfile(mapInProfile(reviewerNetcash.getProfile()));
        dtoInt.setProfessionPosition(reviewerNetcash.getProfessionPosition());
        dtoInt.setRegistrationIdentifier(reviewerNetcash.getRegistrationIdentifier());
        return dtoInt;
    }

    private List<ModelContactDetail> mapInContacDetails(final List<DTOIntContactDetail> contactDetails) {
        if(CollectionUtils.isEmpty(contactDetails)){
            return null;
        }
        return contactDetails.stream().filter(Objects::nonNull).map(this::mapInContacDetail).collect(Collectors.toList());
    }

    private ModelContactDetail mapInContacDetail(final DTOIntContactDetail contactDetail) {
        if(contactDetail == null){
            return null;
        }
        ModelContactDetail dtoInt = new ModelContactDetail();
        dtoInt.setContact(contactDetail.getContact());
        dtoInt.setContactType(translator.translateFrontendEnumValueStrictly(Enums.ENUM_CONTACTDETAILS_CONTACTTYPE_ID, contactDetail.getContactType()));
        return dtoInt;
    }

    private ModelReviewerType mapInReviewerType(final DTOIntReviewerType reviewerType) {
        if(reviewerType == null){
            return null;
        }
        ModelReviewerType dtoInt = new ModelReviewerType();
        dtoInt.setId(translator.translateFrontendEnumValueStrictly(Enums.ENUM_SUSCRIPTIONREQUEST_REVIEWER_ID, reviewerType.getId()));
        return dtoInt;
    }

    private ModelBank mapInBank(final DTOIntBank bank) {
        if(bank == null){
            return null;
        }
        ModelBank dtoInt = new ModelBank();
        dtoInt.setId(bank.getId());
        dtoInt.setBranch(mapInBranch(bank.getBranch()));
        return dtoInt;
    }

    private ModelBranch mapInBranch(final DTOIntBranch branch) {
        if(branch == null){
            return null;
        }
        ModelBranch dtoInt = new ModelBranch();
        dtoInt.setId(branch.getId());
        return dtoInt;
    }

    private ModelProfile mapInProfile(final DTOIntProfile profile) {
        if(profile == null){
            return null;
        }
        ModelProfile dtoInt = new ModelProfile();
        dtoInt.setId(profile.getId());
        return dtoInt;
    }

    private ModelBusiness mapInBusiness(final DTOIntBusiness business) {
        if(business == null){
            return null;
        }
        ModelBusiness dtoInt = new ModelBusiness();
        dtoInt.setBusinessDocuments(mapInBusinessDocuments(business.getBusinessDocuments()));
        dtoInt.setBusinessManagement(mapInBusinessManagement(business.getBusinessManagement()));
        dtoInt.setLimitAmount(mapInLimitAmount(business.getLimitAmount()));
        return dtoInt;
    }

    private List<ModelBusinessDocument> mapInBusinessDocuments(final List<DTOIntBusinessDocument> businessDocuments) {
        if(CollectionUtils.isEmpty(businessDocuments)){
            return null;
        }
        return businessDocuments.stream().filter(Objects::nonNull).map(this::mapInBusinessDocument).collect(Collectors.toList());
    }

    private ModelBusinessDocument mapInBusinessDocument(final DTOIntBusinessDocument businessDocument) {
        if(businessDocument == null){
            return null;
        }
        ModelBusinessDocument dtoInt = new ModelBusinessDocument();
        dtoInt.setBusinessDocumentType(mapInBusinessDocumentType(businessDocument.getBusinessDocumentType()));
        dtoInt.setDocumentNumber(businessDocument.getDocumentNumber());
        dtoInt.setIssueDate(businessDocument.getIssueDate());
        dtoInt.setExpirationDate(businessDocument.getExpirationDate());
        return dtoInt;
    }

    private ModelBusinessDocumentType mapInBusinessDocumentType(final DTOIntBusinessDocumentType businessDocumentType) {
        if(businessDocumentType == null){
            return null;
        }
        ModelBusinessDocumentType dtoInt = new ModelBusinessDocumentType();
        dtoInt.setId(translator.translateFrontendEnumValueStrictly(Enums.ENUM_DOCUMENTTYPE_ID, businessDocumentType.getId()));
        return dtoInt;
    }

    private ModelBusinessManagement mapInBusinessManagement(final DTOIntBusinessManagement businessManagement) {
        if(businessManagement == null){
            return null;
        }
        ModelBusinessManagement dtoInt = new ModelBusinessManagement();
        dtoInt.setManagementType(mapInManagementType(businessManagement.getManagementType()));
        return dtoInt;
    }

    private ModelManagementType mapInManagementType(final DTOIntManagementType managementType) {
        if(managementType == null){
            return null;
        }
        ModelManagementType dtoInt = new ModelManagementType();
        dtoInt.setId(translator.translateFrontendEnumValueStrictly(Enums.ENUM_FINANCIALMANAGEMENTCOMPANY_BUSINESSMANAGEMENT_MANAGEMENTTYPE_ID, managementType.getId()));
        return dtoInt;
    }

    private ModelLimitAmount mapInLimitAmount(final DTOIntLimitAmount limitAmount) {
        if(limitAmount == null){
            return null;
        }
        ModelLimitAmount dtoInt = new ModelLimitAmount();
        dtoInt.setAmount(limitAmount.getAmount());
        dtoInt.setCurrency(limitAmount.getCurrency());
        return dtoInt;
    }

    @Override
    public FinancialManagementCompanies mapOut(final ModelFinancialManagementCompaniesResponse dtoOut) {
        LOG.info("... called method RestCreateFinancialManagementCompanyMapper.mapOut ...");
        if(dtoOut.getData() == null){
            return null;
        }
        FinancialManagementCompanies financialManagementCompanies = new FinancialManagementCompanies();
        financialManagementCompanies.setId(dtoOut.getData().getId());
        return financialManagementCompanies;
    }
}
