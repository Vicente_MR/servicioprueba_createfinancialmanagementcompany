// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.pzic.financialmanagementcompanies.dao.model.kwhb;

import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhb.FormatoKNECBEHB;

privileged aspect FormatoKNECBEHB_Roo_JavaBean {
    
    /**
     * Gets codemp value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getCodemp() {
        return this.codemp;
    }
    
    /**
     * Sets codemp value
     * 
     * @param codemp
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setCodemp(String codemp) {
        this.codemp = codemp;
        return this;
    }
    
    /**
     * Gets cta01 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getCta01() {
        return this.cta01;
    }
    
    /**
     * Sets cta01 value
     * 
     * @param cta01
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setCta01(String cta01) {
        this.cta01 = cta01;
        return this;
    }
    
    /**
     * Gets idepr1 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdepr1() {
        return this.idepr1;
    }
    
    /**
     * Sets idepr1 value
     * 
     * @param idepr1
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdepr1(String idepr1) {
        this.idepr1 = idepr1;
        return this;
    }
    
    /**
     * Gets idtpr1 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdtpr1() {
        return this.idtpr1;
    }
    
    /**
     * Sets idtpr1 value
     * 
     * @param idtpr1
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdtpr1(String idtpr1) {
        this.idtpr1 = idtpr1;
        return this;
    }
    
    /**
     * Gets idrpr1 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdrpr1() {
        return this.idrpr1;
    }
    
    /**
     * Sets idrpr1 value
     * 
     * @param idrpr1
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdrpr1(String idrpr1) {
        this.idrpr1 = idrpr1;
        return this;
    }
    
    /**
     * Gets cta02 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getCta02() {
        return this.cta02;
    }
    
    /**
     * Sets cta02 value
     * 
     * @param cta02
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setCta02(String cta02) {
        this.cta02 = cta02;
        return this;
    }
    
    /**
     * Gets idepr2 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdepr2() {
        return this.idepr2;
    }
    
    /**
     * Sets idepr2 value
     * 
     * @param idepr2
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdepr2(String idepr2) {
        this.idepr2 = idepr2;
        return this;
    }
    
    /**
     * Gets idtpr2 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdtpr2() {
        return this.idtpr2;
    }
    
    /**
     * Sets idtpr2 value
     * 
     * @param idtpr2
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdtpr2(String idtpr2) {
        this.idtpr2 = idtpr2;
        return this;
    }
    
    /**
     * Gets idrpr2 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdrpr2() {
        return this.idrpr2;
    }
    
    /**
     * Sets idrpr2 value
     * 
     * @param idrpr2
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdrpr2(String idrpr2) {
        this.idrpr2 = idrpr2;
        return this;
    }
    
    /**
     * Gets cta03 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getCta03() {
        return this.cta03;
    }
    
    /**
     * Sets cta03 value
     * 
     * @param cta03
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setCta03(String cta03) {
        this.cta03 = cta03;
        return this;
    }
    
    /**
     * Gets idepr3 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdepr3() {
        return this.idepr3;
    }
    
    /**
     * Sets idepr3 value
     * 
     * @param idepr3
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdepr3(String idepr3) {
        this.idepr3 = idepr3;
        return this;
    }
    
    /**
     * Gets idtpr3 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdtpr3() {
        return this.idtpr3;
    }
    
    /**
     * Sets idtpr3 value
     * 
     * @param idtpr3
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdtpr3(String idtpr3) {
        this.idtpr3 = idtpr3;
        return this;
    }
    
    /**
     * Gets idrpr3 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdrpr3() {
        return this.idrpr3;
    }
    
    /**
     * Sets idrpr3 value
     * 
     * @param idrpr3
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdrpr3(String idrpr3) {
        this.idrpr3 = idrpr3;
        return this;
    }
    
    /**
     * Gets cta04 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getCta04() {
        return this.cta04;
    }
    
    /**
     * Sets cta04 value
     * 
     * @param cta04
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setCta04(String cta04) {
        this.cta04 = cta04;
        return this;
    }
    
    /**
     * Gets idepr4 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdepr4() {
        return this.idepr4;
    }
    
    /**
     * Sets idepr4 value
     * 
     * @param idepr4
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdepr4(String idepr4) {
        this.idepr4 = idepr4;
        return this;
    }
    
    /**
     * Gets idtpr4 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdtpr4() {
        return this.idtpr4;
    }
    
    /**
     * Sets idtpr4 value
     * 
     * @param idtpr4
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdtpr4(String idtpr4) {
        this.idtpr4 = idtpr4;
        return this;
    }
    
    /**
     * Gets idrpr4 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdrpr4() {
        return this.idrpr4;
    }
    
    /**
     * Sets idrpr4 value
     * 
     * @param idrpr4
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdrpr4(String idrpr4) {
        this.idrpr4 = idrpr4;
        return this;
    }
    
    /**
     * Gets cta05 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getCta05() {
        return this.cta05;
    }
    
    /**
     * Sets cta05 value
     * 
     * @param cta05
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setCta05(String cta05) {
        this.cta05 = cta05;
        return this;
    }
    
    /**
     * Gets idepr5 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdepr5() {
        return this.idepr5;
    }
    
    /**
     * Sets idepr5 value
     * 
     * @param idepr5
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdepr5(String idepr5) {
        this.idepr5 = idepr5;
        return this;
    }
    
    /**
     * Gets idtpr5 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdtpr5() {
        return this.idtpr5;
    }
    
    /**
     * Sets idtpr5 value
     * 
     * @param idtpr5
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdtpr5(String idtpr5) {
        this.idtpr5 = idtpr5;
        return this;
    }
    
    /**
     * Gets idrpr5 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdrpr5() {
        return this.idrpr5;
    }
    
    /**
     * Sets idrpr5 value
     * 
     * @param idrpr5
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdrpr5(String idrpr5) {
        this.idrpr5 = idrpr5;
        return this;
    }
    
    /**
     * Gets cta06 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getCta06() {
        return this.cta06;
    }
    
    /**
     * Sets cta06 value
     * 
     * @param cta06
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setCta06(String cta06) {
        this.cta06 = cta06;
        return this;
    }
    
    /**
     * Gets idepr6 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdepr6() {
        return this.idepr6;
    }
    
    /**
     * Sets idepr6 value
     * 
     * @param idepr6
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdepr6(String idepr6) {
        this.idepr6 = idepr6;
        return this;
    }
    
    /**
     * Gets idtpr6 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdtpr6() {
        return this.idtpr6;
    }
    
    /**
     * Sets idtpr6 value
     * 
     * @param idtpr6
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdtpr6(String idtpr6) {
        this.idtpr6 = idtpr6;
        return this;
    }
    
    /**
     * Gets idrpr6 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdrpr6() {
        return this.idrpr6;
    }
    
    /**
     * Sets idrpr6 value
     * 
     * @param idrpr6
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdrpr6(String idrpr6) {
        this.idrpr6 = idrpr6;
        return this;
    }
    
    /**
     * Gets cta07 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getCta07() {
        return this.cta07;
    }
    
    /**
     * Sets cta07 value
     * 
     * @param cta07
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setCta07(String cta07) {
        this.cta07 = cta07;
        return this;
    }
    
    /**
     * Gets idepr7 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdepr7() {
        return this.idepr7;
    }
    
    /**
     * Sets idepr7 value
     * 
     * @param idepr7
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdepr7(String idepr7) {
        this.idepr7 = idepr7;
        return this;
    }
    
    /**
     * Gets idtpr7 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdtpr7() {
        return this.idtpr7;
    }
    
    /**
     * Sets idtpr7 value
     * 
     * @param idtpr7
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdtpr7(String idtpr7) {
        this.idtpr7 = idtpr7;
        return this;
    }
    
    /**
     * Gets idrpr7 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdrpr7() {
        return this.idrpr7;
    }
    
    /**
     * Sets idrpr7 value
     * 
     * @param idrpr7
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdrpr7(String idrpr7) {
        this.idrpr7 = idrpr7;
        return this;
    }
    
    /**
     * Gets cta08 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getCta08() {
        return this.cta08;
    }
    
    /**
     * Sets cta08 value
     * 
     * @param cta08
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setCta08(String cta08) {
        this.cta08 = cta08;
        return this;
    }
    
    /**
     * Gets idepr8 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdepr8() {
        return this.idepr8;
    }
    
    /**
     * Sets idepr8 value
     * 
     * @param idepr8
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdepr8(String idepr8) {
        this.idepr8 = idepr8;
        return this;
    }
    
    /**
     * Gets idtpr8 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdtpr8() {
        return this.idtpr8;
    }
    
    /**
     * Sets idtpr8 value
     * 
     * @param idtpr8
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdtpr8(String idtpr8) {
        this.idtpr8 = idtpr8;
        return this;
    }
    
    /**
     * Gets idrpr8 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdrpr8() {
        return this.idrpr8;
    }
    
    /**
     * Sets idrpr8 value
     * 
     * @param idrpr8
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdrpr8(String idrpr8) {
        this.idrpr8 = idrpr8;
        return this;
    }
    
    /**
     * Gets cta09 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getCta09() {
        return this.cta09;
    }
    
    /**
     * Sets cta09 value
     * 
     * @param cta09
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setCta09(String cta09) {
        this.cta09 = cta09;
        return this;
    }
    
    /**
     * Gets idepr9 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdepr9() {
        return this.idepr9;
    }
    
    /**
     * Sets idepr9 value
     * 
     * @param idepr9
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdepr9(String idepr9) {
        this.idepr9 = idepr9;
        return this;
    }
    
    /**
     * Gets idtpr9 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdtpr9() {
        return this.idtpr9;
    }
    
    /**
     * Sets idtpr9 value
     * 
     * @param idtpr9
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdtpr9(String idtpr9) {
        this.idtpr9 = idtpr9;
        return this;
    }
    
    /**
     * Gets idrpr9 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdrpr9() {
        return this.idrpr9;
    }
    
    /**
     * Sets idrpr9 value
     * 
     * @param idrpr9
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdrpr9(String idrpr9) {
        this.idrpr9 = idrpr9;
        return this;
    }
    
    /**
     * Gets cta10 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getCta10() {
        return this.cta10;
    }
    
    /**
     * Sets cta10 value
     * 
     * @param cta10
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setCta10(String cta10) {
        this.cta10 = cta10;
        return this;
    }
    
    /**
     * Gets idepr10 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdepr10() {
        return this.idepr10;
    }
    
    /**
     * Sets idepr10 value
     * 
     * @param idepr10
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdepr10(String idepr10) {
        this.idepr10 = idepr10;
        return this;
    }
    
    /**
     * Gets idtpr10 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdtpr10() {
        return this.idtpr10;
    }
    
    /**
     * Sets idtpr10 value
     * 
     * @param idtpr10
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdtpr10(String idtpr10) {
        this.idtpr10 = idtpr10;
        return this;
    }
    
    /**
     * Gets idrpr10 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdrpr10() {
        return this.idrpr10;
    }
    
    /**
     * Sets idrpr10 value
     * 
     * @param idrpr10
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdrpr10(String idrpr10) {
        this.idrpr10 = idrpr10;
        return this;
    }
    
    /**
     * Gets cta11 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getCta11() {
        return this.cta11;
    }
    
    /**
     * Sets cta11 value
     * 
     * @param cta11
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setCta11(String cta11) {
        this.cta11 = cta11;
        return this;
    }
    
    /**
     * Gets idepr11 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdepr11() {
        return this.idepr11;
    }
    
    /**
     * Sets idepr11 value
     * 
     * @param idepr11
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdepr11(String idepr11) {
        this.idepr11 = idepr11;
        return this;
    }
    
    /**
     * Gets idtpr11 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdtpr11() {
        return this.idtpr11;
    }
    
    /**
     * Sets idtpr11 value
     * 
     * @param idtpr11
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdtpr11(String idtpr11) {
        this.idtpr11 = idtpr11;
        return this;
    }
    
    /**
     * Gets idrpr11 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdrpr11() {
        return this.idrpr11;
    }
    
    /**
     * Sets idrpr11 value
     * 
     * @param idrpr11
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdrpr11(String idrpr11) {
        this.idrpr11 = idrpr11;
        return this;
    }
    
    /**
     * Gets cta12 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getCta12() {
        return this.cta12;
    }
    
    /**
     * Sets cta12 value
     * 
     * @param cta12
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setCta12(String cta12) {
        this.cta12 = cta12;
        return this;
    }
    
    /**
     * Gets idepr12 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdepr12() {
        return this.idepr12;
    }
    
    /**
     * Sets idepr12 value
     * 
     * @param idepr12
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdepr12(String idepr12) {
        this.idepr12 = idepr12;
        return this;
    }
    
    /**
     * Gets idtpr12 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdtpr12() {
        return this.idtpr12;
    }
    
    /**
     * Sets idtpr12 value
     * 
     * @param idtpr12
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdtpr12(String idtpr12) {
        this.idtpr12 = idtpr12;
        return this;
    }
    
    /**
     * Gets idrpr12 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdrpr12() {
        return this.idrpr12;
    }
    
    /**
     * Sets idrpr12 value
     * 
     * @param idrpr12
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdrpr12(String idrpr12) {
        this.idrpr12 = idrpr12;
        return this;
    }
    
    /**
     * Gets cta13 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getCta13() {
        return this.cta13;
    }
    
    /**
     * Sets cta13 value
     * 
     * @param cta13
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setCta13(String cta13) {
        this.cta13 = cta13;
        return this;
    }
    
    /**
     * Gets idepr13 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdepr13() {
        return this.idepr13;
    }
    
    /**
     * Sets idepr13 value
     * 
     * @param idepr13
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdepr13(String idepr13) {
        this.idepr13 = idepr13;
        return this;
    }
    
    /**
     * Gets idtpr13 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdtpr13() {
        return this.idtpr13;
    }
    
    /**
     * Sets idtpr13 value
     * 
     * @param idtpr13
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdtpr13(String idtpr13) {
        this.idtpr13 = idtpr13;
        return this;
    }
    
    /**
     * Gets idrpr13 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdrpr13() {
        return this.idrpr13;
    }
    
    /**
     * Sets idrpr13 value
     * 
     * @param idrpr13
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdrpr13(String idrpr13) {
        this.idrpr13 = idrpr13;
        return this;
    }
    
    /**
     * Gets cta14 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getCta14() {
        return this.cta14;
    }
    
    /**
     * Sets cta14 value
     * 
     * @param cta14
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setCta14(String cta14) {
        this.cta14 = cta14;
        return this;
    }
    
    /**
     * Gets idepr14 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdepr14() {
        return this.idepr14;
    }
    
    /**
     * Sets idepr14 value
     * 
     * @param idepr14
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdepr14(String idepr14) {
        this.idepr14 = idepr14;
        return this;
    }
    
    /**
     * Gets idtpr14 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdtpr14() {
        return this.idtpr14;
    }
    
    /**
     * Sets idtpr14 value
     * 
     * @param idtpr14
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdtpr14(String idtpr14) {
        this.idtpr14 = idtpr14;
        return this;
    }
    
    /**
     * Gets idrpr14 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdrpr14() {
        return this.idrpr14;
    }
    
    /**
     * Sets idrpr14 value
     * 
     * @param idrpr14
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdrpr14(String idrpr14) {
        this.idrpr14 = idrpr14;
        return this;
    }
    
    /**
     * Gets cta15 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getCta15() {
        return this.cta15;
    }
    
    /**
     * Sets cta15 value
     * 
     * @param cta15
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setCta15(String cta15) {
        this.cta15 = cta15;
        return this;
    }
    
    /**
     * Gets idepr15 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdepr15() {
        return this.idepr15;
    }
    
    /**
     * Sets idepr15 value
     * 
     * @param idepr15
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdepr15(String idepr15) {
        this.idepr15 = idepr15;
        return this;
    }
    
    /**
     * Gets idtpr15 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdtpr15() {
        return this.idtpr15;
    }
    
    /**
     * Sets idtpr15 value
     * 
     * @param idtpr15
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdtpr15(String idtpr15) {
        this.idtpr15 = idtpr15;
        return this;
    }
    
    /**
     * Gets idrpr15 value
     * 
     * @return String
     */
    public String FormatoKNECBEHB.getIdrpr15() {
        return this.idrpr15;
    }
    
    /**
     * Sets idrpr15 value
     * 
     * @param idrpr15
     * @return FormatoKNECBEHB
     */
    public FormatoKNECBEHB FormatoKNECBEHB.setIdrpr15(String idrpr15) {
        this.idrpr15 = idrpr15;
        return this;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @return String
     */
    public String FormatoKNECBEHB.toString() {
        return "FormatoKNECBEHB {" + 
                "codemp='" + codemp + '\'' + 
                ", cta01='" + cta01 + '\'' + 
                ", idepr1='" + idepr1 + '\'' + 
                ", idtpr1='" + idtpr1 + '\'' + 
                ", idrpr1='" + idrpr1 + '\'' + 
                ", cta02='" + cta02 + '\'' + 
                ", idepr2='" + idepr2 + '\'' + 
                ", idtpr2='" + idtpr2 + '\'' + 
                ", idrpr2='" + idrpr2 + '\'' + 
                ", cta03='" + cta03 + '\'' + 
                ", idepr3='" + idepr3 + '\'' + 
                ", idtpr3='" + idtpr3 + '\'' + 
                ", idrpr3='" + idrpr3 + '\'' + 
                ", cta04='" + cta04 + '\'' + 
                ", idepr4='" + idepr4 + '\'' + 
                ", idtpr4='" + idtpr4 + '\'' + 
                ", idrpr4='" + idrpr4 + '\'' + 
                ", cta05='" + cta05 + '\'' + 
                ", idepr5='" + idepr5 + '\'' + 
                ", idtpr5='" + idtpr5 + '\'' + 
                ", idrpr5='" + idrpr5 + '\'' + 
                ", cta06='" + cta06 + '\'' + 
                ", idepr6='" + idepr6 + '\'' + 
                ", idtpr6='" + idtpr6 + '\'' + 
                ", idrpr6='" + idrpr6 + '\'' + 
                ", cta07='" + cta07 + '\'' + 
                ", idepr7='" + idepr7 + '\'' + 
                ", idtpr7='" + idtpr7 + '\'' + 
                ", idrpr7='" + idrpr7 + '\'' + 
                ", cta08='" + cta08 + '\'' + 
                ", idepr8='" + idepr8 + '\'' + 
                ", idtpr8='" + idtpr8 + '\'' + 
                ", idrpr8='" + idrpr8 + '\'' + 
                ", cta09='" + cta09 + '\'' + 
                ", idepr9='" + idepr9 + '\'' + 
                ", idtpr9='" + idtpr9 + '\'' + 
                ", idrpr9='" + idrpr9 + '\'' + 
                ", cta10='" + cta10 + '\'' + 
                ", idepr10='" + idepr10 + '\'' + 
                ", idtpr10='" + idtpr10 + '\'' + 
                ", idrpr10='" + idrpr10 + '\'' + 
                ", cta11='" + cta11 + '\'' + 
                ", idepr11='" + idepr11 + '\'' + 
                ", idtpr11='" + idtpr11 + '\'' + 
                ", idrpr11='" + idrpr11 + '\'' + 
                ", cta12='" + cta12 + '\'' + 
                ", idepr12='" + idepr12 + '\'' + 
                ", idtpr12='" + idtpr12 + '\'' + 
                ", idrpr12='" + idrpr12 + '\'' + 
                ", cta13='" + cta13 + '\'' + 
                ", idepr13='" + idepr13 + '\'' + 
                ", idtpr13='" + idtpr13 + '\'' + 
                ", idrpr13='" + idrpr13 + '\'' + 
                ", cta14='" + cta14 + '\'' + 
                ", idepr14='" + idepr14 + '\'' + 
                ", idtpr14='" + idtpr14 + '\'' + 
                ", idrpr14='" + idrpr14 + '\'' + 
                ", cta15='" + cta15 + '\'' + 
                ", idepr15='" + idepr15 + '\'' + 
                ", idtpr15='" + idtpr15 + '\'' + 
                ", idrpr15='" + idrpr15 + '\'' + "}" + super.toString();
    }
    
}
