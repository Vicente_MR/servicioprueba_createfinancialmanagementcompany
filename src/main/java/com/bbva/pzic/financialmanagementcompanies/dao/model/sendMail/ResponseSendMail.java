package com.bbva.pzic.financialmanagementcompanies.dao.model.sendMail;

public class ResponseSendMail {

    private String transactionID;
    private String code;
    private String message;

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
