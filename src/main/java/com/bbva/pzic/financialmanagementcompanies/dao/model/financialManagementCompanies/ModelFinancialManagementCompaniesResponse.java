package com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompanies;

/**
 * Created on 16/12/2020.
 *
 * @author Entelgy
 */
public class ModelFinancialManagementCompaniesResponse {
    private ModelFinancialManagementCompaniesData data;

    public ModelFinancialManagementCompaniesData getData() {
        return data;
    }

    public void setData(ModelFinancialManagementCompaniesData data) {
        this.data = data;
    }
}
