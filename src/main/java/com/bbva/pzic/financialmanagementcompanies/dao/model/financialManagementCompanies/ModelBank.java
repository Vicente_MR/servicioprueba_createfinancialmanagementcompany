package com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompanies;

/**
 * Created on 16/12/2020.
 *
 * @author Entelgy
 */
public class ModelBank {
    private String id;
    private ModelBranch branch;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ModelBranch getBranch() {
        return branch;
    }

    public void setBranch(ModelBranch branch) {
        this.branch = branch;
    }
}
