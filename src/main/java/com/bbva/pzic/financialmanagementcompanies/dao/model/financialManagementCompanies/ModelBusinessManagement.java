package com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompanies;

/**
 * Created on 16/12/2020.
 *
 * @author Entelgy
 */
public class ModelBusinessManagement {
    private ModelManagementType managementType;

    public ModelManagementType getManagementType() {
        return managementType;
    }

    public void setManagementType(ModelManagementType managementType) {
        this.managementType = managementType;
    }
}
