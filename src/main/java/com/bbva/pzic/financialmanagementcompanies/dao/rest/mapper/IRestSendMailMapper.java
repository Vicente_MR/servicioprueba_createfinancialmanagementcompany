package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputSendEmailOtpFinancialManagementCompaniesBusinessManager;
import com.bbva.pzic.financialmanagementcompanies.dao.model.sendMail.ModelSendMail;

public interface IRestSendMailMapper {

    ModelSendMail mapIn(InputSendEmailOtpFinancialManagementCompaniesBusinessManager entityPayload);
}
