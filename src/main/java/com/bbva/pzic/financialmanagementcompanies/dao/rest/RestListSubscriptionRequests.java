package com.bbva.pzic.financialmanagementcompanies.dao.rest;

import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntSubscriptionRequests;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputListSubscriptionRequests;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelSubscriptionRequests;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestListSubscriptionRequestsMapper;
import com.bbva.pzic.financialmanagementcompanies.util.connection.rest.RestGetConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
@Component
public class RestListSubscriptionRequests extends RestGetConnection<ModelSubscriptionRequests> {

    private static final String LIST_SUBSCRIPTION_REQUESTS_URL = "servicing.url.financialManagementCompanies.listSubscriptionRequests";
    private static final String LIST_SUBSCRIPTION_REQUESTS_USE_PROXY = "servicing.proxy.financialManagementCompanies.listSubscriptionRequests";

    @Autowired
    private IRestListSubscriptionRequestsMapper mapper;

    @PostConstruct
    public void init() {
        useProxy = configurationManager.getBooleanProperty(LIST_SUBSCRIPTION_REQUESTS_USE_PROXY, false);
    }

    public DTOIntSubscriptionRequests invoke(final InputListSubscriptionRequests input) {
        return mapper.mapOut(connect(LIST_SUBSCRIPTION_REQUESTS_URL, mapper.mapIn(input)));
    }

    @Override
    protected void evaluateResponse(ModelSubscriptionRequests response, int statusCode) {
        evaluateMessagesResponse(response.getMessages(), "SMCPE1810277", statusCode);
    }
}