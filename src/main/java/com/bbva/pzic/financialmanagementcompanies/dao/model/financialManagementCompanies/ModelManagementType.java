package com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompanies;

/**
 * Created on 16/12/2020.
 *
 * @author Entelgy
 */
public class ModelManagementType {
    private String id;
    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
