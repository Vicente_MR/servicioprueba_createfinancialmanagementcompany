package com.bbva.pzic.financialmanagementcompanies.dao;

import com.bbva.pzic.financialmanagementcompanies.business.dto.*;
import com.bbva.pzic.financialmanagementcompanies.dao.model.ksjo.ResponseData;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.*;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
public interface IFinancialManagementCompaniesDAO {

    String createSubscriptionRequest(SubscriptionRequest dtoInt);

    DTOIntSubscriptionRequests listSubscriptionRequests(
            InputListSubscriptionRequests input);

    SubscriptionRequest getSubscriptionRequest(InputGetSubscriptionRequest input);

    void modifySubscriptionRequest(InputModifySubscriptionRequest input);

    void modifyBusinessManagerSubscriptionRequest(
            InputModifyBusinessManagerSubscriptionRequest input);

    String createReviewerSubscriptionRequest(
            InputCreateReviewerSubscriptionRequest input);

    void createFinancialManagementCompaniesRelatedContracts(
            InputCreateFinancialManagementCompaniesRelatedContracts input);

    String createFinancialManagementCompaniesBusinessManager(
            DTOIntBusinessManager dtoInt);

    CreateAuthorizedBusinessManager createFinancialManagementCompaniesAuthorizedBusinessManager(
            InputCreateAuthorizedBusinessManager input);

    ResponseData newUserSimpleWithPasswordOnce(InputRequestBackendRest input);

    void sendEmail(InputSendEmailOtpFinancialManagementCompaniesBusinessManager input);

    void reactivateUserSimpleWithPasswordOnce(InputRequestBackendRest input);

    InputSendEmailOtpFinancialManagementCompaniesBusinessManager getBusinessManagerData(
            InputSendEmailOtpFinancialManagementCompaniesBusinessManager input);

    void deleteUser(InputRequestBackendRest input);

    void deleteGroup(InputRequestBackendRest input);

    ProfiledService getFinancialManagementCompaniesAuthorizedBusinessManagerProfiledService(
            InputGetFMCAuthorizedBusinessManagerProfiledService input);

    ValidateOperationFeasibility validateFinancialManagementCompaniesAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility(
            InputValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility input);

    DTOIntServiceContract listFinancialManagementCompaniesAuthorizedBusinessManagersProfiledServicesContracts(
            InputListFMCAuthorizedBusinessManagersProfiledServicesContracts input);

    FinancialManagementCompanies createFinancialManagementCompany(
            DTOIntFinancialManagementCompanies input
    );
}
