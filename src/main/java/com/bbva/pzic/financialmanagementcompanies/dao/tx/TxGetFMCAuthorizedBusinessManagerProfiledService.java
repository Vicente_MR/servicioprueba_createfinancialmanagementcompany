package com.bbva.pzic.financialmanagementcompanies.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputGetFMCAuthorizedBusinessManagerProfiledService;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwfp.FormatoKNECFPE0;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwfp.FormatoKNECFPS0;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwfp.PeticionTransaccionKwfp;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwfp.RespuestaTransaccionKwfp;
import com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper.ITxGetFMCAuthorizedBusinessManagerProfiledServiceMapper;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.ProfiledService;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.SingleOutputFormat;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
@Component("txGetFinancialManagementCompaniesAuthorizedBusinessManagerProfiledService")
public class TxGetFMCAuthorizedBusinessManagerProfiledService
        extends SingleOutputFormat<InputGetFMCAuthorizedBusinessManagerProfiledService, FormatoKNECFPE0, ProfiledService, FormatoKNECFPS0> {

    @Resource(name = "txGetFMCAuthorizedBusinessManagerProfiledServiceMapper")
    private ITxGetFMCAuthorizedBusinessManagerProfiledServiceMapper mapper;

    public TxGetFMCAuthorizedBusinessManagerProfiledService(@Qualifier("transaccionKwfp") InvocadorTransaccion<PeticionTransaccionKwfp, RespuestaTransaccionKwfp> transaction) {
        super(transaction, PeticionTransaccionKwfp::new, ProfiledService::new, FormatoKNECFPS0.class);
    }

    @Override
    protected FormatoKNECFPE0 mapInput(InputGetFMCAuthorizedBusinessManagerProfiledService inputGetFMCAuthorizedBusinessManagerProfiledService) {
        return mapper.mapIn(inputGetFMCAuthorizedBusinessManagerProfiledService);
    }

    @Override
    protected ProfiledService mapFirstOutputFormat(FormatoKNECFPS0 formatoKNECFPS0, InputGetFMCAuthorizedBusinessManagerProfiledService inputGetFMCAuthorizedBusinessManagerProfiledService, ProfiledService profiledService) {
        return mapper.mapOut(formatoKNECFPS0);
    }
}
