package com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

/**
 * @author Entelgy
 */
public class ModelBusinessDocumentSubscriptionRequest {

    private ModelBusinessDocumentType businessDocumentType;

    @DatoAuditable(omitir = true)
    private String documentNumber;

    public ModelBusinessDocumentType getBusinessDocumentType() {
        return businessDocumentType;
    }

    public void setBusinessDocumentType(ModelBusinessDocumentType businessDocumentType) {
        this.businessDocumentType = businessDocumentType;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }
}