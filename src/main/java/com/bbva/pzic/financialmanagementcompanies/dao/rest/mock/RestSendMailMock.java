package com.bbva.pzic.financialmanagementcompanies.dao.rest.mock;

import com.bbva.pzic.financialmanagementcompanies.dao.model.sendMail.ModelSendMail;
import com.bbva.pzic.financialmanagementcompanies.dao.model.sendMail.ResponseSendMail;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.RestSendMail;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mock.stubs.ResponseFinancialManagementCompaniesMock;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

/**
 * Created on 26/09/2018.
 *
 * @author Entelgy
 */
@Primary
@Component
public class RestSendMailMock extends RestSendMail {

    @Override
    protected ResponseSendMail connect(String urlPropertyValue, ModelSendMail entityPayload) {
        // evaluateResponse(responseSendMail, 200);
        return ResponseFinancialManagementCompaniesMock.getInstance().buildSenMailResponse();
    }
}
