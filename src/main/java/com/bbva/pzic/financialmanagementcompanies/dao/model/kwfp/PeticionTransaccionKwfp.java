package com.bbva.pzic.financialmanagementcompanies.dao.model.kwfp;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>KWFP</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionKwfp</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionKwfp</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTCCT.KWFP.D1200224.txt
 * KWFPCONS.D OP. X USU.CEF               KN        KN2CKWFP     01 KNECFPE0            KWFP  NS2000ANNNNN    SSTN    C   SNNSNNNN  NN                2020-01-27XP86388 2020-02-2409.52.41XP86388 2020-01-27-16.01.17.701270XP86388 0001-01-010001-01-01
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.KNECFPE0.D1200224.txt
 * KNECFPE0�INPUT:CONS.D OP. X USU.CEF    �F�03�00020�01�00001�COMPAID�CODIGO DE COMPANIA  �A�008�0�R�        �
 * KNECFPE0�INPUT:CONS.D OP. X USU.CEF    �F�03�00020�02�00009�MANAGID�CODIGO DE USUARIO   �A�008�0�R�        �
 * KNECFPE0�INPUT:CONS.D OP. X USU.CEF    �F�03�00020�03�00017�CODSER �CODIGO DE SERVICIO  �A�004�0�R�        �
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.KNECFPS0.D1200224.txt
 * KNECFPS0�OUTPUT:CONS.D OP. X USU.CEF   �X�12�00222�01�00001�IDECON �IDENTIFICADOR CODIGO�A�004�0�S�        �
 * KNECFPS0�OUTPUT:CONS.D OP. X USU.CEF   �X�12�00222�02�00005�SERVIC �SERVICIO            �A�004�0�S�        �
 * KNECFPS0�OUTPUT:CONS.D OP. X USU.CEF   �X�12�00222�03�00009�SERVDES�DESCRIPCION DE SERVI�A�040�0�S�        �
 * KNECFPS0�OUTPUT:CONS.D OP. X USU.CEF   �X�12�00222�04�00049�INDSER �INDICAD.DE SRVICIO  �A�002�0�S�        �
 * KNECFPS0�OUTPUT:CONS.D OP. X USU.CEF   �X�12�00222�05�00051�DESIND �DESCRIPC.DE INDICADO�A�040�0�S�        �
 * KNECFPS0�OUTPUT:CONS.D OP. X USU.CEF   �X�12�00222�06�00091�PERMIDE�PERMISO INDICADOR   �A�002�0�S�        �
 * KNECFPS0�OUTPUT:CONS.D OP. X USU.CEF   �X�12�00222�07�00093�PERMDES�TIPO PERMIS.DESCRIPC�A�050�0�S�        �
 * KNECFPS0�OUTPUT:CONS.D OP. X USU.CEF   �X�12�00222�08�00143�POWERID�PODER DE FIRMA OPERA�A�002�0�S�        �
 * KNECFPS0�OUTPUT:CONS.D OP. X USU.CEF   �X�12�00222�09�00145�POWEDES�DESCRIPC.PODER FIRMA�A�040�0�S�        �
 * KNECFPS0�OUTPUT:CONS.D OP. X USU.CEF   �X�12�00222�10�00185�AMOUNT �MAXIMO MONTO PERMITI�N�017�2�S�        �
 * KNECFPS0�OUTPUT:CONS.D OP. X USU.CEF   �X�12�00222�11�00202�CURENCY�MONTO CURRENCY      �A�020�0�S�        �
 * KNECFPS0�OUTPUT:CONS.D OP. X USU.CEF   �X�12�00222�12�00222�EMPAUD �EMP.CON O SIN AUDITO�A�001�0�S�        �
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.KWFP.D1200224.txt
 * KWFPKNECFPS0COPY    KN2CKWFP1S0155S000                     XP86388 2020-02-20-14.42.44.318533XP86388 2020-02-20-14.42.44.318571
 *
</pre></code>
 *
 * @see RespuestaTransaccionKwfp
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "KWFP",
	tipo = 1,
	subtipo = 1,
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionKwfp.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoKNECFPE0.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionKwfp implements MensajeMultiparte {

	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}

}
