package com.bbva.pzic.financialmanagementcompanies.dao.rest;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputRequestBackendRest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.ksjo.BodyDataRest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.ksjo.ResponseData;
import com.bbva.pzic.financialmanagementcompanies.dao.model.validator.ResponseDataValidator;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.impl.RestReactivationUserSimpleMapper;
import com.bbva.pzic.financialmanagementcompanies.util.connection.rest.RestPostConnection;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Collections;
import java.util.Map;

/**
 * Created on 26/09/2018.
 *
 * @author Entelgy
 */
@Component
public class RestReactivationUserSimpleWithPasswordOnce extends RestPostConnection<BodyDataRest, ResponseData> {

    private static final String URL = "servicing.url.financialManagementCompanies.sendEmailOtpBusinessManager.reactivationUserSimpleWithPasswordOnce";
    private static final String USE_PROXY = "servicing.proxy.financialManagementCompanies.sendEmailOtpBusinessManager.reactivationUserSimpleWithPasswordOnce";

    @Resource(name = "reactivationUserSimpleMapper")
    private RestReactivationUserSimpleMapper reactivationUserSimpleMapper;

    @Resource
    private ResponseDataValidator restStatusValidator;

    @PostConstruct
    public void init() {
        useProxy = configurationManager.getBooleanProperty(USE_PROXY, false);
    }

    public void invoke(InputRequestBackendRest entityPayload) {
        Map<String, String> headerParams = reactivationUserSimpleMapper.mapInHeader(entityPayload);
        BodyDataRest bodyDataRest = reactivationUserSimpleMapper.mapIn(entityPayload);
        connect(URL, null, null, headerParams, bodyDataRest);
    }

    @Override
    protected void evaluateResponse(ResponseData response, int statusCode) {
        evaluateMessagesResponse(Collections.singletonList(
                restStatusValidator.buildMessage(response)), "SMCPE1810337",
                restStatusValidator.buildStatusCode(response, statusCode));
    }
}
