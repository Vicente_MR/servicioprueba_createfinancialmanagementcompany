package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper;

import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntFinancialManagementCompanies;
import com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompanies.ModelFinancialManagementCompaniesRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompanies.ModelFinancialManagementCompaniesResponse;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.FinancialManagementCompanies;

/**
 * Created on 16/12/2020.
 *
 * @author Entelgy
 */
public interface IRestCreateFinancialManagementCompanyMapper {
    ModelFinancialManagementCompaniesRequest mapIn(DTOIntFinancialManagementCompanies dtoInt);

    FinancialManagementCompanies mapOut(ModelFinancialManagementCompaniesResponse dtoOut);
}
