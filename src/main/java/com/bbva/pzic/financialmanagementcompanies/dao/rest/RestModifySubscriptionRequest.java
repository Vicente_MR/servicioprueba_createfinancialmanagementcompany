package com.bbva.pzic.financialmanagementcompanies.dao.rest;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputModifySubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelSubscriptionResponse;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestModifySubscriptionRequestMapper;
import com.bbva.pzic.financialmanagementcompanies.util.connection.rest.RestPutConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
@Component
public class RestModifySubscriptionRequest extends RestPutConnection<ModelSubscriptionRequest, ModelSubscriptionResponse> {

    private static final String MODIFY_SUBSCRIPTION_REQUEST_URL = "servicing.url.financialManagementCompanies.modifySubscriptionRequest";
    private static final String MODIFY_SUBSCRIPTION_REQUEST_USE_PROXY = "servicing.proxy.financialManagementCompanies.modifySubscriptionRequest";

    @Autowired
    private IRestModifySubscriptionRequestMapper mapper;

    @PostConstruct
    public void init() {
        useProxy = configurationManager.getBooleanProperty(MODIFY_SUBSCRIPTION_REQUEST_USE_PROXY, false);
    }

    public void invoke(final InputModifySubscriptionRequest input) {
        connect(MODIFY_SUBSCRIPTION_REQUEST_URL, mapper.mapInPath(input), mapper.mapIn(input));
    }

    @Override
    protected void evaluateResponse(ModelSubscriptionResponse response, int statusCode) {
        evaluateMessagesResponse(response.getMessages(), "SMCPE1810279", statusCode);
    }
}