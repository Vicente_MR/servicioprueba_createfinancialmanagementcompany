package com.bbva.pzic.financialmanagementcompanies.dao.model.kwhf;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Invocador de la transacci&oacute;n <code>KWHF</code>
 *
 * @see PeticionTransaccionKwhf
 * @see RespuestaTransaccionKwhf
 */
@Component("transaccionKwhf")
public class TransaccionKwhf implements InvocadorTransaccion<PeticionTransaccionKwhf, RespuestaTransaccionKwhf> {

    @Autowired
    private ServicioTransacciones servicioTransacciones;

    @Override
    public RespuestaTransaccionKwhf invocar(PeticionTransaccionKwhf transaccion) throws ExcepcionTransaccion {
        return servicioTransacciones.invocar(PeticionTransaccionKwhf.class, RespuestaTransaccionKwhf.class, transaccion);
    }

    @Override
    public RespuestaTransaccionKwhf invocarCache(PeticionTransaccionKwhf transaccion) throws ExcepcionTransaccion {
        return servicioTransacciones.invocar(PeticionTransaccionKwhf.class, RespuestaTransaccionKwhf.class, transaccion);
    }

    @Override
    public void vaciarCache() {
    }
}