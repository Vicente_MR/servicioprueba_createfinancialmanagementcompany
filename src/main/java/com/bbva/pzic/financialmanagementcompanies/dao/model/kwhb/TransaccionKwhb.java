package com.bbva.pzic.financialmanagementcompanies.dao.model.kwhb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>KWHB</code>
 * 
 * @see PeticionTransaccionKwhb
 * @see RespuestaTransaccionKwhb
 */
@Component
public class TransaccionKwhb implements InvocadorTransaccion<PeticionTransaccionKwhb,RespuestaTransaccionKwhb> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionKwhb invocar(PeticionTransaccionKwhb transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionKwhb.class, RespuestaTransaccionKwhb.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionKwhb invocarCache(PeticionTransaccionKwhb transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionKwhb.class, RespuestaTransaccionKwhb.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {}	
}