package com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompanies;

import java.util.Date;

/**
 * Created on 16/12/2020.
 *
 * @author Entelgy
 */
public class ModelBusinessDocument {
    private ModelBusinessDocumentType businessDocumentType;
    private String documentNumber;
    private Date issueDate;
    private Date expirationDate;

    public ModelBusinessDocumentType getBusinessDocumentType() {
        return businessDocumentType;
    }

    public void setBusinessDocumentType(ModelBusinessDocumentType businessDocumentType) {
        this.businessDocumentType = businessDocumentType;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }
}
