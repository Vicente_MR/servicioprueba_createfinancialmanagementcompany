package com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests;

/**
 * @author Entelgy
 */
public class ModelContactDetail {

    private String contactType;
    private String contact;

    public String getContactType() {
        return contactType;
    }

    public void setContactType(String contactType) {
        this.contactType = contactType;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }
}