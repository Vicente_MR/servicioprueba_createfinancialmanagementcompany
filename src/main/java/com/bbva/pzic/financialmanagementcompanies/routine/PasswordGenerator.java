package com.bbva.pzic.financialmanagementcompanies.routine;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.financialmanagementcompanies.util.Errors;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.EnumMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Component
public class PasswordGenerator {

    private static final Log LOG = LogFactory.getLog(PasswordGenerator.class);

    private static final String HASHING_CODE = "smc.configuration.SMCPE1810337.password.hashing.code";
    private static final String HASHING_DIGITS = "smc.configuration.SMCPE1810337.password.hashing.digits";
    private static final String HASHING_SECRET_KEY = "smc.configuration.SMCPE1810337.password.hashing.secretKey";
    private static final String HASHING_CHARSET = "smc.configuration.SMCPE1810337.password.hashing.charset";

    @Autowired
    private EnumMapper enumMapper;

    public String generatePassword(final String aap) {
        String code = enumMapper.getPropertyValueApp(aap, HASHING_CODE);
        String digits = enumMapper.getPropertyValueApp(aap, HASHING_DIGITS);
        String secretKey = enumMapper.getPropertyValueApp(aap, HASHING_SECRET_KEY);
        String charset = enumMapper.getPropertyValueApp(aap, HASHING_CHARSET);

        if (code == null) {
            LOG.error("El dato 'algorithm code' es obligatorio");
            throw new BusinessServiceException(Errors.MANDATORY_PARAMETERS_MISSING);
        }
        if (digits == null) {
            LOG.error("El dato 'digits' es obligatorio");
            throw new BusinessServiceException(Errors.MANDATORY_PARAMETERS_MISSING);
        }
        if (secretKey == null) {
            LOG.error("El dato 'secretKey' es obligatorio");
            throw new BusinessServiceException(Errors.MANDATORY_PARAMETERS_MISSING);
        }
        if (charset == null) {
            LOG.error("El dato 'charset' es obligatorio");
            throw new BusinessServiceException(Errors.MANDATORY_PARAMETERS_MISSING);
        }

        try {
            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getBytes(charset), code);

            Mac mac = Mac.getInstance(code);
            mac.init(secretKeySpec);

            ByteBuffer buffer = ByteBuffer.wrap(mac.doFinal(
                    String.valueOf(System.currentTimeMillis()).getBytes(charset)));

            int value;
            int index = 0;
            int length = Integer.parseInt(digits);
            String generated;
            do {
                value = buffer.getInt(index);
                index++;
                generated = String.valueOf(value);
            } while (value < 0 || generated.length() < length);

            generated = generated.substring(0, length);
            LOG.debug(String.format("Password generated: %s", generated));
            return generated;
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException | InvalidKeyException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }
    }
}
