package com.bbva.pzic.financialmanagementcompanies.util;

public class Enums {
    public static final String ENUM_DOCUMENTTYPE_ID = "documentType.id";
    public static final String ENUM_FINANCIALMANAGEMENTCOMPANY_BUSINESSMANAGEMENT_MANAGEMENTTYPE_ID = "financialManagementCompany.businessManagement.managementType.id";
    public static final String ENUM_SUSCRIPTIONREQUEST_PRODUCT_ID = "suscriptionRequest.product.id";
    public static final String ENUM_FINANCIALMANAGEMENTCOMPANY_VERSION_ID = "financialManagementCompany.version.id";
    public static final String ENUM_FINANCIALMANAGEMENTCOMPANY_PRODUCTTYPE_ID = "financialManagementCompany.productType.id";
    public static final String ENUM_FINANCIALMANAGEMENTCOMPANY_RELATEDCONTRACTS_RELATIONTYPE = "financialManagementCompany.relatedContracts.relationType";
    public static final String ENUM_CONTACTDETAILS_CONTACTTYPE_ID = "contactDetails.contactType.id";
    public static final String ENUM_SUSCRIPTIONREQUEST_REVIEWER_ID = "suscriptionRequest.reviewer.id";

    public Enums() {
    }

}
