package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputCreateFinancialManagementCompaniesRelatedContracts;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.RelatedContract;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.EnumMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 13/09/2018.
 *
 * @author Entelgy
 */
public class CreateFinancialManagementCompaniesRelatedContractsMapperTest {

    @InjectMocks
    private CreateFinancialManagementCompaniesRelatedContractsMapper mapper;

    @Mock
    private EnumMapper enumMapper;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mapBackendValues();
    }

    private void mapBackendValues() {
        Mockito.when(enumMapper.getBackendValue("productType.id", EntityStubs.PRODUCT_TYPE_ID_C)).thenReturn("C");
        Mockito.when(enumMapper.getBackendValue("productType.id", EntityStubs.PRODUCT_TYPE_ID_T)).thenReturn("T");
        Mockito.when(enumMapper.getBackendValue("productType.id", EntityStubs.PRODUCT_TYPE_ID_P)).thenReturn("P");
        Mockito.when(enumMapper.getBackendValue("productType.id", EntityStubs.PRODUCT_TYPE_ID_F)).thenReturn("F");
        Mockito.when(enumMapper.getBackendValue("productType.id", EntityStubs.PRODUCT_TYPE_ID_D)).thenReturn("D");
        Mockito.when(enumMapper.getBackendValue("relationType.id", EntityStubs.RELATION_TYPE_ID_C)).thenReturn("C");
        Mockito.when(enumMapper.getBackendValue("relationType.id", EntityStubs.RELATION_TYPE_ID_R)).thenReturn("R");
    }

    @Test
    public void mapInFullTest() throws IOException {
        List<RelatedContract> input = EntityStubs.getInstance().getRelatedContracts();
        InputCreateFinancialManagementCompaniesRelatedContracts result = mapper.mapIn(EntityStubs.FINANCIAL_MANAGEMENT_COMPANY_ID, input);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getFinancialManagementCompanyId());
        Assert.assertNotNull(result.getRelatedContracts().get(0).getContract().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(0).getProduct().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(0).getProduct().getProductType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(0).getRelationType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(1).getContract().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(1).getProduct().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(1).getProduct().getProductType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(1).getRelationType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(2).getContract().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(2).getProduct().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(2).getProduct().getProductType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(2).getRelationType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(3).getContract().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(3).getProduct().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(3).getProduct().getProductType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(3).getRelationType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(4).getContract().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(4).getProduct().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(4).getProduct().getProductType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(4).getRelationType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(5).getContract().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(5).getProduct().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(5).getProduct().getProductType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(5).getRelationType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(6).getContract().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(6).getProduct().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(6).getProduct().getProductType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(6).getRelationType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(7).getContract().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(7).getProduct().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(7).getProduct().getProductType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(7).getRelationType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(8).getContract().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(8).getProduct().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(8).getProduct().getProductType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(8).getRelationType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(9).getContract().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(9).getProduct().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(9).getProduct().getProductType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(9).getRelationType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(10).getContract().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(10).getProduct().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(10).getProduct().getProductType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(10).getRelationType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(11).getContract().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(11).getProduct().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(11).getProduct().getProductType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(11).getRelationType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(12).getContract().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(12).getProduct().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(12).getProduct().getProductType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(12).getRelationType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(13).getContract().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(13).getProduct().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(13).getProduct().getProductType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(13).getRelationType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(14).getContract().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(14).getProduct().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(14).getProduct().getProductType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(14).getRelationType().getId());

        Assert.assertEquals(EntityStubs.FINANCIAL_MANAGEMENT_COMPANY_ID, result.getFinancialManagementCompanyId());
        Assert.assertEquals(input.get(0).getContract().getId(), result.getRelatedContracts().get(0).getContract().getId());
        Assert.assertEquals(input.get(0).getProduct().getId(), result.getRelatedContracts().get(0).getProduct().getId());
        Assert.assertEquals(EntityStubs.PRODUCT_TYPE_BACKEND_C, result.getRelatedContracts().get(0).getProduct().getProductType().getId());
        Assert.assertEquals(EntityStubs.RELATION_TYPE_BACKEND_C, result.getRelatedContracts().get(0).getRelationType().getId());
        Assert.assertEquals(input.get(1).getContract().getId(), result.getRelatedContracts().get(1).getContract().getId());
        Assert.assertEquals(input.get(1).getProduct().getId(), result.getRelatedContracts().get(1).getProduct().getId());
        Assert.assertEquals(EntityStubs.PRODUCT_TYPE_BACKEND_T, result.getRelatedContracts().get(1).getProduct().getProductType().getId());
        Assert.assertEquals(EntityStubs.RELATION_TYPE_BACKEND_C, result.getRelatedContracts().get(1).getRelationType().getId());
        Assert.assertEquals(input.get(2).getContract().getId(), result.getRelatedContracts().get(2).getContract().getId());
        Assert.assertEquals(input.get(2).getProduct().getId(), result.getRelatedContracts().get(2).getProduct().getId());
        Assert.assertEquals(EntityStubs.PRODUCT_TYPE_BACKEND_P, result.getRelatedContracts().get(2).getProduct().getProductType().getId());
        Assert.assertEquals(EntityStubs.RELATION_TYPE_BACKEND_R, result.getRelatedContracts().get(2).getRelationType().getId());
        Assert.assertEquals(input.get(3).getContract().getId(), result.getRelatedContracts().get(3).getContract().getId());
        Assert.assertEquals(input.get(3).getProduct().getId(), result.getRelatedContracts().get(3).getProduct().getId());
        Assert.assertEquals(EntityStubs.PRODUCT_TYPE_BACKEND_F, result.getRelatedContracts().get(3).getProduct().getProductType().getId());
        Assert.assertEquals(EntityStubs.RELATION_TYPE_BACKEND_R, result.getRelatedContracts().get(3).getRelationType().getId());
        Assert.assertEquals(input.get(4).getContract().getId(), result.getRelatedContracts().get(4).getContract().getId());
        Assert.assertEquals(input.get(4).getProduct().getId(), result.getRelatedContracts().get(4).getProduct().getId());
        Assert.assertEquals(EntityStubs.PRODUCT_TYPE_BACKEND_D, result.getRelatedContracts().get(4).getProduct().getProductType().getId());
        Assert.assertEquals(EntityStubs.RELATION_TYPE_BACKEND_C, result.getRelatedContracts().get(4).getRelationType().getId());
        Assert.assertEquals(input.get(5).getContract().getId(), result.getRelatedContracts().get(5).getContract().getId());
        Assert.assertEquals(input.get(5).getProduct().getId(), result.getRelatedContracts().get(5).getProduct().getId());
        Assert.assertEquals(EntityStubs.PRODUCT_TYPE_BACKEND_C, result.getRelatedContracts().get(5).getProduct().getProductType().getId());
        Assert.assertEquals(EntityStubs.RELATION_TYPE_BACKEND_C, result.getRelatedContracts().get(5).getRelationType().getId());
        Assert.assertEquals(input.get(6).getContract().getId(), result.getRelatedContracts().get(6).getContract().getId());
        Assert.assertEquals(input.get(6).getProduct().getId(), result.getRelatedContracts().get(6).getProduct().getId());
        Assert.assertEquals(EntityStubs.PRODUCT_TYPE_BACKEND_T, result.getRelatedContracts().get(6).getProduct().getProductType().getId());
        Assert.assertEquals(EntityStubs.RELATION_TYPE_BACKEND_R, result.getRelatedContracts().get(6).getRelationType().getId());
        Assert.assertEquals(input.get(7).getContract().getId(), result.getRelatedContracts().get(7).getContract().getId());
        Assert.assertEquals(input.get(7).getProduct().getId(), result.getRelatedContracts().get(7).getProduct().getId());
        Assert.assertEquals(EntityStubs.PRODUCT_TYPE_BACKEND_P, result.getRelatedContracts().get(7).getProduct().getProductType().getId());
        Assert.assertEquals(EntityStubs.RELATION_TYPE_BACKEND_R, result.getRelatedContracts().get(7).getRelationType().getId());
        Assert.assertEquals(input.get(8).getContract().getId(), result.getRelatedContracts().get(8).getContract().getId());
        Assert.assertEquals(input.get(8).getProduct().getId(), result.getRelatedContracts().get(8).getProduct().getId());
        Assert.assertEquals(EntityStubs.PRODUCT_TYPE_BACKEND_F, result.getRelatedContracts().get(8).getProduct().getProductType().getId());
        Assert.assertEquals(EntityStubs.RELATION_TYPE_BACKEND_C, result.getRelatedContracts().get(8).getRelationType().getId());
        Assert.assertEquals(input.get(9).getContract().getId(), result.getRelatedContracts().get(9).getContract().getId());
        Assert.assertEquals(input.get(9).getProduct().getId(), result.getRelatedContracts().get(9).getProduct().getId());
        Assert.assertEquals(EntityStubs.PRODUCT_TYPE_BACKEND_D, result.getRelatedContracts().get(9).getProduct().getProductType().getId());
        Assert.assertEquals(EntityStubs.RELATION_TYPE_BACKEND_C, result.getRelatedContracts().get(9).getRelationType().getId());
        Assert.assertEquals(input.get(10).getContract().getId(), result.getRelatedContracts().get(10).getContract().getId());
        Assert.assertEquals(input.get(10).getProduct().getId(), result.getRelatedContracts().get(10).getProduct().getId());
        Assert.assertEquals(EntityStubs.PRODUCT_TYPE_BACKEND_C, result.getRelatedContracts().get(10).getProduct().getProductType().getId());
        Assert.assertEquals(EntityStubs.RELATION_TYPE_BACKEND_R, result.getRelatedContracts().get(10).getRelationType().getId());
        Assert.assertEquals(input.get(11).getContract().getId(), result.getRelatedContracts().get(11).getContract().getId());
        Assert.assertEquals(input.get(11).getProduct().getId(), result.getRelatedContracts().get(11).getProduct().getId());
        Assert.assertEquals(EntityStubs.PRODUCT_TYPE_BACKEND_T, result.getRelatedContracts().get(11).getProduct().getProductType().getId());
        Assert.assertEquals(EntityStubs.RELATION_TYPE_BACKEND_R, result.getRelatedContracts().get(11).getRelationType().getId());
        Assert.assertEquals(input.get(12).getContract().getId(), result.getRelatedContracts().get(12).getContract().getId());
        Assert.assertEquals(input.get(12).getProduct().getId(), result.getRelatedContracts().get(12).getProduct().getId());
        Assert.assertEquals(EntityStubs.PRODUCT_TYPE_BACKEND_P, result.getRelatedContracts().get(12).getProduct().getProductType().getId());
        Assert.assertEquals(EntityStubs.RELATION_TYPE_BACKEND_C, result.getRelatedContracts().get(12).getRelationType().getId());
        Assert.assertEquals(input.get(13).getContract().getId(), result.getRelatedContracts().get(13).getContract().getId());
        Assert.assertEquals(input.get(13).getProduct().getId(), result.getRelatedContracts().get(13).getProduct().getId());
        Assert.assertEquals(EntityStubs.PRODUCT_TYPE_BACKEND_F, result.getRelatedContracts().get(13).getProduct().getProductType().getId());
        Assert.assertEquals(EntityStubs.RELATION_TYPE_BACKEND_C, result.getRelatedContracts().get(13).getRelationType().getId());
        Assert.assertEquals(input.get(14).getContract().getId(), result.getRelatedContracts().get(14).getContract().getId());
        Assert.assertEquals(input.get(14).getProduct().getId(), result.getRelatedContracts().get(14).getProduct().getId());
        Assert.assertEquals(EntityStubs.PRODUCT_TYPE_BACKEND_D, result.getRelatedContracts().get(14).getProduct().getProductType().getId());
        Assert.assertEquals(EntityStubs.RELATION_TYPE_BACKEND_R, result.getRelatedContracts().get(14).getRelationType().getId());
    }

    @Test
    public void mapInEmptyTest() {
        List<RelatedContract> relatedContracts = new ArrayList<>();
        RelatedContract relatedContract = new RelatedContract();
        relatedContracts.add(relatedContract);

        InputCreateFinancialManagementCompaniesRelatedContracts result = mapper.mapIn(EntityStubs.FINANCIAL_MANAGEMENT_COMPANY_ID, relatedContracts);
        Assert.assertNotNull(result);
        Assert.assertNull(result.getRelatedContracts());
        Assert.assertNull(result.getFinancialManagementCompanyId());
    }
}
