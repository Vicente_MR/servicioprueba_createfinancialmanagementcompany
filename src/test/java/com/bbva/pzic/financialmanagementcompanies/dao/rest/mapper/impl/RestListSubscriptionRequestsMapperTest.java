package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntSubscriptionRequests;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputListSubscriptionRequests;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelSubscriptionRequests;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mock.stubs.ResponseFinancialManagementCompaniesMock;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.EnumMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.HashMap;

import static com.bbva.pzic.financialmanagementcompanies.EntityStubs.*;
import static org.junit.Assert.*;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class RestListSubscriptionRequestsMapperTest {

    @InjectMocks
    private RestListSubscriptionRequestsMapper mapper;
    @Mock
    private EnumMapper enumMapper;

    @Test
    public void mapInFullTest() {
        final InputListSubscriptionRequests input = EntityStubs.getInstance().getInputListSubscriptionRequests();
        final HashMap<String, String> result = mapper.mapIn(input);
        assertFalse(result.isEmpty());
        assertNotNull(result.get(RestListSubscriptionRequestsMapper.SUBSCRIPTIONS_REQUEST_ID));
        assertNotNull(result.get(RestListSubscriptionRequestsMapper.BUSINESS_DOCUMENTS_BUSINESS_DOCUMENT_TYPE_ID));
        assertNotNull(result.get(RestListSubscriptionRequestsMapper.BUSINESS_DOCUMENTS_DOCUMENT_NUMBER));
        assertNotNull(result.get(RestListSubscriptionRequestsMapper.STATUS_ID));
        assertNotNull(result.get(RestListSubscriptionRequestsMapper.UNIT_MANAGEMENT));
        assertNotNull(result.get(RestListSubscriptionRequestsMapper.BRANCH_ID));
        assertNotNull(result.get(RestListSubscriptionRequestsMapper.FROM_SUBSCRIPTION_REQUEST_DATE));
        assertNotNull(result.get(RestListSubscriptionRequestsMapper.TO_SUBSCRIPTION_REQUEST_DATE));
        assertNotNull(result.get(RestListSubscriptionRequestsMapper.BUSINESS_ID));
        assertNotNull(result.get(RestListSubscriptionRequestsMapper.PAGINATION_KEY));
        assertNotNull(result.get(RestListSubscriptionRequestsMapper.PAGE_SIZE));

        assertEquals(input.getSubscriptionsRequestId(), result.get(RestListSubscriptionRequestsMapper.SUBSCRIPTIONS_REQUEST_ID));
        assertEquals(input.getBusinessDocumentsBusinessDocumentTypeId(), result.get(RestListSubscriptionRequestsMapper.BUSINESS_DOCUMENTS_BUSINESS_DOCUMENT_TYPE_ID));
        assertEquals(input.getBusinessDocumentsDocumentNumber(), result.get(RestListSubscriptionRequestsMapper.BUSINESS_DOCUMENTS_DOCUMENT_NUMBER));
        assertEquals(input.getStatusId(), result.get(RestListSubscriptionRequestsMapper.STATUS_ID));
        assertEquals(input.getUnitManagement(), result.get(RestListSubscriptionRequestsMapper.UNIT_MANAGEMENT));
        assertEquals(input.getBranchId(), result.get(RestListSubscriptionRequestsMapper.BRANCH_ID));
        assertEquals(input.getFromSubscriptionRequestDate(), result.get(RestListSubscriptionRequestsMapper.FROM_SUBSCRIPTION_REQUEST_DATE));
        assertEquals(input.getToSubscriptionRequestDate(), result.get(RestListSubscriptionRequestsMapper.TO_SUBSCRIPTION_REQUEST_DATE));
        assertEquals(input.getBusinessId(), result.get(RestListSubscriptionRequestsMapper.BUSINESS_ID));
        assertEquals(input.getPaginationKey(), result.get(RestListSubscriptionRequestsMapper.PAGINATION_KEY));
        assertEquals(input.getPageSize().toString(), result.get(RestListSubscriptionRequestsMapper.PAGE_SIZE));
    }

    @Test
    public void mapInWithoutPageSizeTest() {
        final InputListSubscriptionRequests input = EntityStubs.getInstance().getInputListSubscriptionRequests();
        input.setPageSize(null);
        final HashMap<String, String> result = mapper.mapIn(input);
        assertFalse(result.isEmpty());
        assertNotNull(result.get(RestListSubscriptionRequestsMapper.SUBSCRIPTIONS_REQUEST_ID));
        assertNotNull(result.get(RestListSubscriptionRequestsMapper.BUSINESS_DOCUMENTS_BUSINESS_DOCUMENT_TYPE_ID));
        assertNotNull(result.get(RestListSubscriptionRequestsMapper.BUSINESS_DOCUMENTS_DOCUMENT_NUMBER));
        assertNotNull(result.get(RestListSubscriptionRequestsMapper.STATUS_ID));
        assertNotNull(result.get(RestListSubscriptionRequestsMapper.UNIT_MANAGEMENT));
        assertNotNull(result.get(RestListSubscriptionRequestsMapper.BRANCH_ID));
        assertNotNull(result.get(RestListSubscriptionRequestsMapper.FROM_SUBSCRIPTION_REQUEST_DATE));
        assertNotNull(result.get(RestListSubscriptionRequestsMapper.TO_SUBSCRIPTION_REQUEST_DATE));
        assertNotNull(result.get(RestListSubscriptionRequestsMapper.BUSINESS_ID));
        assertNotNull(result.get(RestListSubscriptionRequestsMapper.PAGINATION_KEY));
        assertNull(result.get(RestListSubscriptionRequestsMapper.PAGE_SIZE));

        assertEquals(input.getSubscriptionsRequestId(), result.get(RestListSubscriptionRequestsMapper.SUBSCRIPTIONS_REQUEST_ID));
        assertEquals(input.getBusinessDocumentsBusinessDocumentTypeId(), result.get(RestListSubscriptionRequestsMapper.BUSINESS_DOCUMENTS_BUSINESS_DOCUMENT_TYPE_ID));
        assertEquals(input.getBusinessDocumentsDocumentNumber(), result.get(RestListSubscriptionRequestsMapper.BUSINESS_DOCUMENTS_DOCUMENT_NUMBER));
        assertEquals(input.getStatusId(), result.get(RestListSubscriptionRequestsMapper.STATUS_ID));
        assertEquals(input.getUnitManagement(), result.get(RestListSubscriptionRequestsMapper.UNIT_MANAGEMENT));
        assertEquals(input.getBranchId(), result.get(RestListSubscriptionRequestsMapper.BRANCH_ID));
        assertEquals(input.getFromSubscriptionRequestDate(), result.get(RestListSubscriptionRequestsMapper.FROM_SUBSCRIPTION_REQUEST_DATE));
        assertEquals(input.getToSubscriptionRequestDate(), result.get(RestListSubscriptionRequestsMapper.TO_SUBSCRIPTION_REQUEST_DATE));
        assertEquals(input.getBusinessId(), result.get(RestListSubscriptionRequestsMapper.BUSINESS_ID));
        assertEquals(input.getPaginationKey(), result.get(RestListSubscriptionRequestsMapper.PAGINATION_KEY));
    }

    @Test
    public void mapInEmptyTest() {
        final HashMap<String, String> result = mapper.mapIn(new InputListSubscriptionRequests());
        assertFalse(result.isEmpty());
        assertNull(result.get(RestListSubscriptionRequestsMapper.SUBSCRIPTIONS_REQUEST_ID));
        assertNull(result.get(RestListSubscriptionRequestsMapper.BUSINESS_DOCUMENTS_BUSINESS_DOCUMENT_TYPE_ID));
        assertNull(result.get(RestListSubscriptionRequestsMapper.BUSINESS_DOCUMENTS_DOCUMENT_NUMBER));
        assertNull(result.get(RestListSubscriptionRequestsMapper.STATUS_ID));
        assertNull(result.get(RestListSubscriptionRequestsMapper.UNIT_MANAGEMENT));
        assertNull(result.get(RestListSubscriptionRequestsMapper.BRANCH_ID));
        assertNull(result.get(RestListSubscriptionRequestsMapper.FROM_SUBSCRIPTION_REQUEST_DATE));
        assertNull(result.get(RestListSubscriptionRequestsMapper.TO_SUBSCRIPTION_REQUEST_DATE));
        assertNull(result.get(RestListSubscriptionRequestsMapper.BUSINESS_ID));
        assertNull(result.get(RestListSubscriptionRequestsMapper.PAGINATION_KEY));
        assertNull(result.get(RestListSubscriptionRequestsMapper.PAGE_SIZE));
    }

    @Test
    public void mapOutFullTest() throws IOException {
        Mockito.when(enumMapper.getEnumValue("subscriptionRequests.documentType.id", RUC_DOCUMENT_TYPE_ID_BACKEND)).thenReturn(RUC_DOCUMENT_TYPE_ID_ENUM);
        Mockito.when(enumMapper.getEnumValue("suscriptionRequest.status.id", APPROVED_SUSCRIPTION_REQUEST_STATUS_ID_BACKEND)).thenReturn(APPROVED_SUSCRIPTION_REQUEST_STATUS_ID_ENUM);
        Mockito.when(enumMapper.getEnumValue("suscriptionRequest.status.reason.id", INVALID_DOCUMENT_NUMBER_SUSCRIPTION_REQUEST_STATUS_REASON_ID_BACKEND)).thenReturn(INVALID_DOCUMENT_NUMBER_SUSCRIPTION_REQUEST_STATUS_REASON_ID_ENUM);
        Mockito.when(enumMapper.getEnumValue("suscriptionRequest.reviewer.id", EXECUTIVE_SUSCRIPTION_REQUEST_REVIEWER_BACKEND)).thenReturn(EXECUTIVE_SUSCRIPTION_REQUEST_REVIEWER_ENUM);

        ModelSubscriptionRequests response = ResponseFinancialManagementCompaniesMock.getInstance().buildResponseModelSubscriptionRequests();
        DTOIntSubscriptionRequests result = mapper.mapOut(response);
        assertNotNull(result);

        assertNotNull(result.getData().get(0).getId());
        assertNotNull(result.getData().get(0).getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getId());
        assertNotNull(result.getData().get(0).getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getName());
        assertNotNull(result.getData().get(0).getBusiness().getBusinessDocuments().get(0).getDocumentNumber());
        assertNotNull(result.getData().get(0).getBusiness().getLegalName());
        assertNotNull(result.getData().get(0).getBusiness().getId());
        assertNotNull(result.getData().get(0).getStatus().getId());
        assertNotNull(result.getData().get(0).getStatus().getName());
        assertNotNull(result.getData().get(0).getStatus().getReason());
        assertNotNull(result.getData().get(0).getOpeningDate());
        assertNotNull(result.getData().get(0).getUnitManagement());
        assertNotNull(result.getData().get(0).getBranch().getId());
        assertNotNull(result.getData().get(0).getBranch().getName());
        assertNotNull(result.getData().get(0).getReviewers().get(0).getBusinessAgentId());
        assertNotNull(result.getData().get(0).getReviewers().get(0).getFirstName());
        assertNotNull(result.getData().get(0).getReviewers().get(0).getMiddleName());
        assertNotNull(result.getData().get(0).getReviewers().get(0).getLastName());
        assertNotNull(result.getData().get(0).getReviewers().get(0).getSecondLastName());
        assertNotNull(result.getData().get(0).getReviewers().get(0).getReviewerType().getId());
        assertNotNull(result.getData().get(0).getReviewers().get(0).getReviewerType().getName());

        assertNotNull(result.getPagination().getPaginationKey());
        assertNotNull(result.getPagination().getPageSize());
        assertNotNull(result.getPagination().getPrevious());
        assertNotNull(result.getPagination().getPage());
        assertNotNull(result.getPagination().getTotalElements());
        assertNotNull(result.getPagination().getTotalPages());

        assertEquals(response.getData().get(0).getId(), result.getData().get(0).getId());
        assertEquals(RUC_DOCUMENT_TYPE_ID_ENUM, result.getData().get(0).getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getId());
        assertEquals(response.getData().get(0).getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getName(), result.getData().get(0).getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getName());
        assertEquals(response.getData().get(0).getBusiness().getBusinessDocuments().get(0).getDocumentNumber(), result.getData().get(0).getBusiness().getBusinessDocuments().get(0).getDocumentNumber());
        assertEquals(response.getData().get(0).getBusiness().getLegalName(), result.getData().get(0).getBusiness().getLegalName());
        assertEquals(response.getData().get(0).getBusiness().getId(), result.getData().get(0).getBusiness().getId());
        assertEquals(APPROVED_SUSCRIPTION_REQUEST_STATUS_ID_ENUM, result.getData().get(0).getStatus().getId());
        assertEquals(response.getData().get(0).getStatus().getName(), result.getData().get(0).getStatus().getName());
        assertEquals(INVALID_DOCUMENT_NUMBER_SUSCRIPTION_REQUEST_STATUS_REASON_ID_ENUM, result.getData().get(0).getStatus().getReason());
        assertEquals(response.getData().get(0).getUnitManagement(), result.getData().get(0).getUnitManagement());
        assertEquals(response.getData().get(0).getBranch().getId(), result.getData().get(0).getBranch().getId());
        assertEquals(response.getData().get(0).getBranch().getName(), result.getData().get(0).getBranch().getName());
        assertEquals(result.getData().get(0).getReviewers().get(0).getBusinessAgentId(), response.getData().get(0).getParticipants().get(0).getCode());
        assertEquals(result.getData().get(0).getReviewers().get(0).getFirstName(), response.getData().get(0).getParticipants().get(0).getFirstName());
        assertEquals(result.getData().get(0).getReviewers().get(0).getMiddleName(), response.getData().get(0).getParticipants().get(0).getMiddleName());
        assertEquals(result.getData().get(0).getReviewers().get(0).getLastName(), response.getData().get(0).getParticipants().get(0).getLastName());
        assertEquals(result.getData().get(0).getReviewers().get(0).getSecondLastName(), response.getData().get(0).getParticipants().get(0).getMotherLastName());
        assertEquals(result.getData().get(0).getReviewers().get(0).getReviewerType().getId(), response.getData().get(0).getParticipants().get(0).getParticipantType().getId());
        assertEquals(result.getData().get(0).getReviewers().get(0).getReviewerType().getName(), response.getData().get(0).getParticipants().get(0).getParticipantType().getName());

        assertEquals(result.getPagination().getPaginationKey(), response.getPagination().getPaginationKey());
        assertEquals(result.getPagination().getPageSize(), response.getPagination().getPageSize());
        assertEquals(result.getPagination().getPrevious(), response.getPagination().getPrevious());
        assertEquals(result.getPagination().getPage(), response.getPagination().getPage());
        assertEquals(result.getPagination().getTotalElements(), response.getPagination().getTotalElements());
        assertEquals(result.getPagination().getTotalPages(), response.getPagination().getTotalPages());
    }

    @Test
    public void mapOutFullWithoutPaginationTest() throws IOException {
        ModelSubscriptionRequests response = ResponseFinancialManagementCompaniesMock.getInstance().buildResponseModelSubscriptionRequests();
        response.setPagination(null);

        DTOIntSubscriptionRequests result = mapper.mapOut(response);
        assertNotNull(result);
        assertNull(result.getPagination());
    }

    @Test
    public void mapOutFullWithoutBusinessDocumentsTest() throws IOException {
        ModelSubscriptionRequests response = ResponseFinancialManagementCompaniesMock.getInstance().buildResponseModelSubscriptionRequests();
        response.getData().get(0).getBusiness().setBusinessDocuments(null);

        DTOIntSubscriptionRequests result = mapper.mapOut(response);
        assertNotNull(result);
        assertNotNull(result.getData());
        assertNotNull(result.getData().get(0).getBusiness());
        assertNull(result.getData().get(0).getBusiness().getBusinessDocuments());
    }

    @Test
    public void mapOutFullWithoutReviewersTest() throws IOException {
        ModelSubscriptionRequests response = ResponseFinancialManagementCompaniesMock.getInstance().buildResponseModelSubscriptionRequests();
        response.getData().get(0).setParticipants(null);

        DTOIntSubscriptionRequests result = mapper.mapOut(response);
        assertNotNull(result);
        assertNotNull(result.getData());
        assertNotNull(result.getData().get(0));
        assertNull(result.getData().get(0).getReviewers());
    }

    @Test
    public void mapOutEmptyTest() {
        DTOIntSubscriptionRequests result = mapper.mapOut(new ModelSubscriptionRequests());
        assertNull(result);
    }
}
