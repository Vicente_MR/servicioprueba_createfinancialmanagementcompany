package com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputCreateAuthorizedBusinessManager;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhe.FormatoKNECBEHE;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhe.FormatoKNECBSHE;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhe.mock.FormatsKwheMock;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.CreateAuthorizedBusinessManager;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.EnumMapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created on 18/09/2018.
 *
 * @author Entelgy
 */
public class TxCreateFinancialManagementCompaniesAuthorizedBusinessManagerMapperTest {

    private static final String AUTHORIZED_BUSINESS_MANAGER_SIGNATURE_ID_ENUM = "JOINT_SIGNATURE_OF_TWO";
    private static final String AUTHORIZED_BUSINESS_MANAGER_OPERATIONS_RIGHTS_PERMISSION_TYPE_ID_ENUM = "SIGNATURE";
    private static final String AUTHORIZED_BUSINESS_MANAGER_STATUS_ID_ENUM = "ACTIVATION";
    @InjectMocks
    private TxCreateFinancialManagementCompaniesAuthorizedBusinessManagerMapper mapper;
    @Mock
    private EnumMapper enumMapper;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void mapInFullTest() throws IOException {
        InputCreateAuthorizedBusinessManager input = EntityStubs.getInstance().getInputCreateFinancialManagementCompaniesAuthorizedBusinessManager();
        FormatoKNECBEHE result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getCodemp());
        assertNotNull(result.getUsuid());
        assertNotNull(result.getPodval());
        assertNotNull(result.getTipper());
        assertNotNull(result.getEstger());

        assertEquals(input.getFinancialManagementCompanyId(), result.getCodemp());
        assertEquals(input.getBusinessManagerId(), result.getUsuid());
        assertEquals(input.getValidationRightsId(), result.getPodval());
        assertEquals(input.getPermissionTypeId(), result.getTipper());
        assertEquals(input.getBusinessManagementStatus(), result.getEstger());
    }

    @Test
    public void mapInEmptyTest() {
        FormatoKNECBEHE result = mapper.mapIn(new InputCreateAuthorizedBusinessManager());
        assertNotNull(result);
        assertNull(result.getCodemp());
        assertNull(result.getUsuid());
        assertNull(result.getPodval());
        assertNull(result.getTipper());
        assertNull(result.getEstger());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        FormatoKNECBSHE format = FormatsKwheMock.getInstance().getFormatoKNECBSHE();
        when(enumMapper.getEnumValue("authorizedBusinessManager.signature.id", format.getPodvals()))
                .thenReturn(AUTHORIZED_BUSINESS_MANAGER_SIGNATURE_ID_ENUM);
        when(enumMapper.getEnumValue("authorizedBusinessManager.operationsRights.permissionType.id", format.getTippers()))
                .thenReturn(AUTHORIZED_BUSINESS_MANAGER_OPERATIONS_RIGHTS_PERMISSION_TYPE_ID_ENUM);
        when(enumMapper.getEnumValue("authorizedBusinessManager.status", format.getGerneg()))
                .thenReturn(AUTHORIZED_BUSINESS_MANAGER_STATUS_ID_ENUM);

        CreateAuthorizedBusinessManager result = mapper.mapOut(format);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getBusinessManager().getId());
        assertNotNull(result.getBusinessManagement().getOperationsRights().getSignature().getValidationRights().getId());
        assertNotNull(result.getBusinessManagement().getOperationsRights().getPermissionType().getId());
        assertNotNull(result.getBusinessManagement().getUsersAdministrationRights().getId());
        assertNotNull(result.getBusinessManagement().getStatus());


        assertEquals(format.getUsuids(), result.getId());
        assertEquals(format.getUsuids(), result.getBusinessManager().getId());
        assertEquals(AUTHORIZED_BUSINESS_MANAGER_SIGNATURE_ID_ENUM, result.getBusinessManagement().getOperationsRights().getSignature().getValidationRights().getId());
        assertEquals(AUTHORIZED_BUSINESS_MANAGER_OPERATIONS_RIGHTS_PERMISSION_TYPE_ID_ENUM, result.getBusinessManagement().getOperationsRights().getPermissionType().getId());
        assertEquals(AUTHORIZED_BUSINESS_MANAGER_SIGNATURE_ID_ENUM, result.getBusinessManagement().getUsersAdministrationRights().getId());
        assertEquals(AUTHORIZED_BUSINESS_MANAGER_STATUS_ID_ENUM, result.getBusinessManagement().getStatus());

    }

    @Test
    public void mapOutEmptyTest() {
        CreateAuthorizedBusinessManager result = mapper.mapOut(new FormatoKNECBSHE());
        assertNotNull(result);
        assertNull(result.getId());
        assertNull(result.getBusinessManager());
        assertNull(result.getBusinessManagement());
    }
}
