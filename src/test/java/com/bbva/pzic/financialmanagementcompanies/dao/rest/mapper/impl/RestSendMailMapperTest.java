package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.impl;

import com.bbva.jee.arq.spring.core.servicing.context.BackendContext;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputSendEmailOtpFinancialManagementCompaniesBusinessManager;
import com.bbva.pzic.financialmanagementcompanies.dao.model.sendMail.ModelSendMail;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.EnumMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class RestSendMailMapperTest {

    private static final String AAP = "sendEmailOtpFinancialManagementCompaniesBusinessManager";
    private static final String SUBJECT = "Net cash - Creacion de usuario administrado";
    private static final String EMAIL = "procesos@bbva.com";

    @InjectMocks
    private RestSendMailMapper mapper;

    @Mock
    private ServiceInvocationContext serviceInvocationContext;

    @Mock
    private EnumMapper enumMapper;

    @Test
    public void mapIn() {
        Mockito.when(serviceInvocationContext.getProperty(BackendContext.AAP)).thenReturn(AAP);
        Mockito.when(enumMapper.getPropertyValueApp(AAP, RestSendMailMapper.EMAIL_SUBJECT)).thenReturn(SUBJECT);
        Mockito.when(enumMapper.getPropertyValueApp(AAP, RestSendMailMapper.EMAIL_SENDER)).thenReturn(EMAIL);

        InputSendEmailOtpFinancialManagementCompaniesBusinessManager input = EntityStubs.getInstance().getInputSendMailRequest();
        ModelSendMail result = mapper.mapIn(input);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getId());
        Assert.assertNotNull(result.getSubject());
        Assert.assertNotNull(result.getSender());
        Assert.assertNotNull(result.getRecipients());
        Assert.assertNotNull(result.getMessageBody());
        Assert.assertNotNull(result.getMessageBody().get(0));
        Assert.assertNotNull(result.getMessageBody().get(1));
        Assert.assertNotNull(result.getMessageBody().get(2));
        Assert.assertNotNull(result.getMessageBody().get(3));
        Assert.assertNotNull(result.getMessageBody().get(4));
        Assert.assertNotNull(result.getMessageBody().get(5));
        Assert.assertNotNull(result.getCodeTemplate());

        Assert.assertEquals(input.getBusinessManagerId(), result.getId());
        Assert.assertNotNull(EMAIL, result.getSender());
        Assert.assertNotNull(SUBJECT, result.getSubject());
        Assert.assertEquals(input.getEmailList().get(0).getValue(), result.getRecipients().get(0));
        Assert.assertEquals("001".concat(input.getUserName()), result.getMessageBody().get(0));
        Assert.assertEquals("003".concat(input.getPassword()), result.getMessageBody().get(2));
        Assert.assertEquals("006".concat(input.getBusinessName()), result.getMessageBody().get(5));
    }

    @Test
    public void mapInEmptyTest() {
        ModelSendMail result = mapper.mapIn(new InputSendEmailOtpFinancialManagementCompaniesBusinessManager());

        Assert.assertNotNull(result);

        Assert.assertNull(result.getId());
        Assert.assertNull(result.getSubject());
        Assert.assertNull(result.getSender());
        Assert.assertNotNull(result.getRecipients());
        Assert.assertNotNull(result.getMessageBody());
        Assert.assertNotNull(result.getCodeTemplate());

        Assert.assertEquals(0, result.getRecipients().size());
        Assert.assertEquals(0, result.getMessageBody().size());
    }

}
