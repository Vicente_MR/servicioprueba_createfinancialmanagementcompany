package com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntBusinessManager;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhd.FormatoKNECBEHD;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhd.FormatoKNECBSHD;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhd.mock.FormatsKwhdMock;
import com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper.ITxCreateFinancialManagementCompaniesBusinessManagerMapper;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * Created on 18/09/2018.
 *
 * @author Entelgy
 */
public class TxCreateFinancialManagementCompaniesBusinessManagerMapperTest {

    private ITxCreateFinancialManagementCompaniesBusinessManagerMapper mapper;

    @Before
    public void setUp() {
        mapper = new TxCreateFinancialManagementCompaniesBusinessManagerMapper();
    }

    @Test
    public void mapInFullTest() throws IOException {
        DTOIntBusinessManager input = EntityStubs.getInstance().getDTOIntBusinessManager();
        FormatoKNECBEHD result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getCodemp());
        assertNotNull(result.getNomusu());
        assertNotNull(result.getDocid());
        assertNotNull(result.getNrodid());
        assertNotNull(result.getTipco0());
        assertNotNull(result.getDaco0());
        assertNotNull(result.getTipco1());
        assertNotNull(result.getDaco1());
        assertNotNull(result.getTipco2());
        assertNotNull(result.getDaco2());
        assertNotNull(result.getTipco3());
        assertNotNull(result.getDaco3());
        assertNotNull(result.getTipus1());
        assertNotNull(result.getTipus2());
        assertNotNull(result.getTipus3());

        assertEquals(input.getFinancialManagementCompanyId(), result.getCodemp());
        assertEquals(input.getFullName(), result.getNomusu());
        assertEquals(input.getIdentityDocumentDocumentTypeId(), result.getDocid());
        assertEquals(input.getIdentityDocumentDocumentNumber(), result.getNrodid());
        assertEquals(input.getContactDetailsContactType0(), result.getTipco0());
        assertEquals(input.getContactDetailsContact0(), result.getDaco0());
        assertEquals(input.getContactDetailsContactType1(), result.getTipco1());
        assertEquals(input.getContactDetailsContact1(), result.getDaco1());
        assertEquals(input.getContactDetailsContactType2(), result.getTipco2());
        assertEquals(input.getContactDetailsContact2(), result.getDaco2());
        assertEquals(input.getContactDetailsContactType3(), result.getTipco3());
        assertEquals(input.getContactDetailsContact3(), result.getDaco3());
        assertEquals(input.getRolesId0(), result.getTipus1());
        assertEquals(input.getRolesId1(), result.getTipus2());
        assertEquals(input.getRolesId2(), result.getTipus3());
    }

    @Test
    public void mapInEmptyTest() {
        FormatoKNECBEHD result = mapper.mapIn(new DTOIntBusinessManager());

        assertNotNull(result);
        assertNull(result.getCodemp());
        assertNull(result.getNomusu());
        assertNull(result.getDocid());
        assertNull(result.getNrodid());
        assertNull(result.getTipco0());
        assertNull(result.getDaco0());
        assertNull(result.getTipco1());
        assertNull(result.getDaco1());
        assertNull(result.getTipco2());
        assertNull(result.getDaco2());
        assertNull(result.getTipco3());
        assertNull(result.getDaco3());
        assertNull(result.getTipus1());
        assertNull(result.getTipus2());
        assertNull(result.getTipus3());
    }

    @Test
    public void mapOutFullTest() {
        FormatoKNECBSHD format = FormatsKwhdMock.getInstance().getFormatoKNECBSHD();
        String result = mapper.mapOut(format);

        assertNotNull(result);
        assertEquals(format.getUsuid(), result);
    }

    @Test
    public void mapOutEmptyTest() {
        String result = mapper.mapOut(new FormatoKNECBSHD());
        assertNull(result);
    }
}
