package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntFinancialManagementCompanies;
import com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompanies.ModelFinancialManagementCompaniesRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompanies.ModelFinancialManagementCompaniesResponse;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestCreateFinancialManagementCompanyMapper;

import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.FinancialManagementCompanies;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * Created on 17/12/2020.
 *
 * @author Entelgy
 */
public class RestCreateManagementCompanyMapperTest {

    @Mock
    private Translator translator;

    @InjectMocks
    IRestCreateFinancialManagementCompanyMapper mapper = new RestCreateFinancialManagementCompanyMapper();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    public void mapInEnumMapper(){
        Mockito.when(translator.translateFrontendEnumValueStrictly("documentType.id", "RUC")).thenReturn("R");
        Mockito.when(translator.translateFrontendEnumValueStrictly("financialManagementCompany.businessManagement.managementType.id", "JOINT")).thenReturn("C");
        Mockito.when(translator.translateFrontendEnumValueStrictly("suscriptionRequest.product.id", "NETCASH_BUSINESS")).thenReturn("E");
        Mockito.when(translator.translateFrontendEnumValueStrictly("financialManagementCompany.version.id", "ADVANCED")).thenReturn("A");
        Mockito.when(translator.translateFrontendEnumValueStrictly("financialManagementCompany.productType.id", "ACCOUNTS")).thenReturn("A");
        Mockito.when(translator.translateFrontendEnumValueStrictly("financialManagementCompany.relatedContracts.relationType", "PAYING_ACCOUNT")).thenReturn("C");
        Mockito.when(translator.translateFrontendEnumValueStrictly("contactDetails.contactType.id", "EMAIL")).thenReturn("MA");
        Mockito.when(translator.translateFrontendEnumValueStrictly("suscriptionRequest.reviewer.id", "EXECUTIVE")).thenReturn("E");

    }

    @Test
    public void mapInFullTest() throws IOException {
        DTOIntFinancialManagementCompanies input = EntityStubs.getInstance().getDTOIntFinancialManagementCompanies();
        mapInEnumMapper();
        ModelFinancialManagementCompaniesRequest result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getId());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getDocumentNumber());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getIssueDate());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getExpirationDate());
        assertNotNull(result.getBusiness().getBusinessManagement().getManagementType().getId());
        assertNotNull(result.getBusiness().getLimitAmount().getAmount());
        assertNotNull(result.getBusiness().getLimitAmount().getCurrency());
        assertNotNull(result.getNetcashType().getId());
        assertNotNull(result.getNetcashType().getVersion().getId());
        assertNotNull(result.getContract().getId());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getProduct().getProductType().getId());
        assertNotNull(result.getRelationType().getId());
        assertNotNull(result.getReviewers().get(0).getBusinessAgentId());
        assertNotNull(result.getReviewers().get(0).getContactDetails().get(0).getContact());
        assertNotNull(result.getReviewers().get(0).getContactDetails().get(0).getContactType());
        assertNotNull(result.getReviewers().get(0).getReviewerType().getId());
        assertNotNull(result.getReviewers().get(0).getUnitManagement());
        assertNotNull(result.getReviewers().get(0).getBank().getId());
        assertNotNull(result.getReviewers().get(0).getBank().getBranch().getId());
        assertNotNull(result.getReviewers().get(0).getProfile().getId());
        assertNotNull(result.getReviewers().get(0).getProfessionPosition());
        assertNotNull(result.getReviewers().get(0).getRegistrationIdentifier());

        assertEquals("R", result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getId());
        assertEquals(input.getBusiness().getBusinessDocuments().get(0).getDocumentNumber(), result.getBusiness().getBusinessDocuments().get(0).getDocumentNumber());
        assertEquals(input.getBusiness().getBusinessDocuments().get(0).getIssueDate(), result.getBusiness().getBusinessDocuments().get(0).getIssueDate());
        assertEquals(input.getBusiness().getBusinessDocuments().get(0).getExpirationDate(), result.getBusiness().getBusinessDocuments().get(0).getExpirationDate());
        assertEquals("C", result.getBusiness().getBusinessManagement().getManagementType().getId());
        assertEquals(input.getBusiness().getLimitAmount().getAmount(), result.getBusiness().getLimitAmount().getAmount());
        assertEquals(input.getBusiness().getLimitAmount().getCurrency(), result.getBusiness().getLimitAmount().getCurrency());
        assertEquals("E", result.getNetcashType().getId());
        assertEquals("A", result.getNetcashType().getVersion().getId());
        assertEquals("111", result.getContract().getId());
        assertEquals("123", result.getProduct().getId());
        assertEquals("A", result.getProduct().getProductType().getId());
        assertEquals("C", result.getRelationType().getId());
        assertEquals(input.getReviewers().get(0).getBusinessAgentId(), result.getReviewers().get(0).getBusinessAgentId());
        assertEquals(input.getReviewers().get(0).getContactDetails().get(0).getContact(), result.getReviewers().get(0).getContactDetails().get(0).getContact());
        assertEquals("MA", result.getReviewers().get(0).getContactDetails().get(0).getContactType());
        assertEquals("E", result.getReviewers().get(0).getReviewerType().getId());
        assertEquals(input.getReviewers().get(0).getUnitManagement(), result.getReviewers().get(0).getUnitManagement());
        assertEquals(input.getReviewers().get(0).getBank().getId(), result.getReviewers().get(0).getBank().getId());
        assertEquals(input.getReviewers().get(0).getBank().getBranch().getId(), result.getReviewers().get(0).getBank().getBranch().getId());
        assertEquals(input.getReviewers().get(0).getProfile().getId(), result.getReviewers().get(0).getProfile().getId());
        assertEquals(input.getReviewers().get(0).getProfessionPosition(), result.getReviewers().get(0).getProfessionPosition());
        assertEquals(input.getReviewers().get(0).getRegistrationIdentifier(), result.getReviewers().get(0).getRegistrationIdentifier());

    }

    @Test
    public void mapInEmptyTest() throws IOException {
        ModelFinancialManagementCompaniesRequest result = mapper.mapIn(new DTOIntFinancialManagementCompanies());
        assertNotNull(result);
        assertNull(result.getBusiness());
        assertNull(result.getNetcashType());
        assertNull(result.getRelationType());
        assertNull(result.getContract());
        assertNull(result.getProduct());
        assertNull(result.getReviewers());
    }

    @Test
    public void mapOputFullTest() throws IOException{
        FinancialManagementCompanies result = mapper.mapOut(EntityStubs.getInstance().getModelFinancialManagementCompaniesResponse());
        assertNotNull(result);
        assertNotNull(result.getId());
    }

    @Test
    public void mapOutEmptyTest() throws IOException {
        FinancialManagementCompanies result = mapper.mapOut(new ModelFinancialManagementCompaniesResponse());
        assertNull(result);
    }
}
