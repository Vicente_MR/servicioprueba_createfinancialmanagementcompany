package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputModifyBusinessManagerSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.BackendParamsNames;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestModifyBusinessManagerSubscriptionRequestMapper;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created on 14/08/2018.
 *
 * @author Entelgy
 */
public class RestModifyBusinessManagerSubscriptionRequestMapperTest {

    private IRestModifyBusinessManagerSubscriptionRequestMapper mapper;

    @Before
    public void setUp() {
        mapper = new RestModifyBusinessManagerSubscriptionRequestMapper();
    }

    @Test
    public void mapInPathParamsTest() {
        final InputModifyBusinessManagerSubscriptionRequest input = EntityStubs.getInstance().getInputModifyBusinessManagerSubscriptionRequest();
        Map<String, String> result = mapper.mapInPathParams(input);

        assertNotNull(result.get(BackendParamsNames.SUBSCRIPTION_REQUEST_ID));
        assertNotNull(result.get(BackendParamsNames.BUSINESS_MANAGER_ID));

        assertEquals(input.getSubscriptionRequestId(), result.get(BackendParamsNames.SUBSCRIPTION_REQUEST_ID));
        assertEquals(input.getBusinessManagerId(), result.get(BackendParamsNames.BUSINESS_MANAGER_ID));
    }

    @Test
    public void mapInFullTest() {
        final InputModifyBusinessManagerSubscriptionRequest input = EntityStubs.getInstance().getInputModifyBusinessManagerSubscriptionRequest();
        final ModelSubscriptionRequest result = mapper.mapIn(input);
        assertNotNull(result);

        assertNull(result.getForm());
        assertNull(result.getBusinessManager().getUserId());
        assertNotNull(result.getBusinessManager().getUserIdHost());

        assertEquals(input.getBusinessManagerSubscriptionRequest().getTargetUserId(), result.getBusinessManager().getUserIdHost());
    }

    @Test
    public void mapInEmptyTest() {
        final InputModifyBusinessManagerSubscriptionRequest input = EntityStubs.getInstance().getInputModifyBusinessManagerSubscriptionRequest();
        input.getBusinessManagerSubscriptionRequest().setTargetUserId(null);

        final ModelSubscriptionRequest result = mapper.mapIn(input);
        assertNotNull(result);

        assertNull(result.getForm());
        assertNull(result.getBusinessManager());
    }
}
