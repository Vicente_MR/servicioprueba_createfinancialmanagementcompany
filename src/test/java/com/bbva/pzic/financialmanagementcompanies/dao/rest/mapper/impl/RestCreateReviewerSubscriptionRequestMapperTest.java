package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputCreateReviewerSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.BackendParamsNames;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelReviewerSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelReviewerSubscriptionResponse;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestCreateReviewerSubscriptionRequestMapper;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mock.stubs.ResponseFinancialManagementCompaniesMock;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created on 14/08/2018.
 *
 * @author Entelgy
 */
public class RestCreateReviewerSubscriptionRequestMapperTest {

    private IRestCreateReviewerSubscriptionRequestMapper mapper;

    @Before
    public void setUp() {
        mapper = new RestCreateReviewerSubscriptionRequestMapper();
    }

    @Test
    public void mapInPathParamsFullTest() throws IOException {
        InputCreateReviewerSubscriptionRequest input = EntityStubs.getInstance()
                .getInputCreateReviewerSubscriptionRequest();

        Map<String, String> result = mapper.mapInPathParams(input);

        assertNotNull(result.get(BackendParamsNames.SUBSCRIPTION_REQUEST_ID));
        assertEquals(input.getSubscriptionRequestId(), result.get(BackendParamsNames.SUBSCRIPTION_REQUEST_ID));
    }

    @Test
    public void mapInFullTest() throws IOException {
        InputCreateReviewerSubscriptionRequest input = EntityStubs.getInstance()
                .getInputCreateReviewerSubscriptionRequest();
        ModelReviewerSubscriptionRequest result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getForm().getId());
        assertNotNull(result.getParticipant().getCode());
        assertNotNull(result.getParticipantType().getId());
        assertNotNull(result.getFirstName());
        assertNotNull(result.getMiddleName());
        assertNotNull(result.getLastName());
        assertNotNull(result.getMotherLastName());
        assertNotNull(result.getContactInformations().get(0).getId());
        assertNotNull(result.getContactInformations().get(0).getContact());
    }

    @Test
    public void mapInWithBusinessAgentIdEmptyTest() throws IOException {
        InputCreateReviewerSubscriptionRequest input = EntityStubs.getInstance()
                .getInputCreateReviewerSubscriptionRequest();
        input.getReviewer().setBusinessAgentId(null);
        ModelReviewerSubscriptionRequest result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getForm().getId());
        assertNull(result.getParticipant());
        assertNotNull(result.getParticipantType().getId());
        assertNotNull(result.getFirstName());
        assertNotNull(result.getMiddleName());
        assertNotNull(result.getLastName());
        assertNotNull(result.getMotherLastName());
        assertNotNull(result.getContactInformations().get(0).getId());
        assertNotNull(result.getContactInformations().get(0).getContact());
    }


    @Test
    public void mapInWithReviewerTypeEmptyTest() throws IOException {

        InputCreateReviewerSubscriptionRequest input = EntityStubs.getInstance()
                .getInputCreateReviewerSubscriptionRequest();
        input.getReviewer().setReviewerType(null);

        ModelReviewerSubscriptionRequest result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getForm().getId());
        assertNotNull(result.getParticipant().getCode());
        assertNull(result.getParticipantType());
        assertNotNull(result.getFirstName());
        assertNotNull(result.getMiddleName());
        assertNotNull(result.getLastName());
        assertNotNull(result.getMotherLastName());
        assertNotNull(result.getContactInformations().get(0).getId());
        assertNotNull(result.getContactInformations().get(0).getContact());
    }

    @Test
    public void mapInWithReviewerTypeIdEmptyTest() throws IOException {

        InputCreateReviewerSubscriptionRequest input = EntityStubs.getInstance()
                .getInputCreateReviewerSubscriptionRequest();
        input.getReviewer().getReviewerType().setId(null);

        ModelReviewerSubscriptionRequest result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getForm().getId());
        assertNotNull(result.getParticipant().getCode());
        assertNull(result.getParticipantType());
        assertNotNull(result.getFirstName());
        assertNotNull(result.getMiddleName());
        assertNotNull(result.getLastName());
        assertNotNull(result.getMotherLastName());
        assertNotNull(result.getContactInformations().get(0).getId());
        assertNotNull(result.getContactInformations().get(0).getContact());
    }

    @Test
    public void mapInWithContactInformationEmptyTest() throws IOException {

        InputCreateReviewerSubscriptionRequest input = EntityStubs.getInstance()
                .getInputCreateReviewerSubscriptionRequest();
        input.getReviewer().setContactDetails(null);
        ModelReviewerSubscriptionRequest result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getForm().getId());
        assertNotNull(result.getParticipant().getCode());
        assertNotNull(result.getParticipantType().getId());
        assertNotNull(result.getFirstName());
        assertNotNull(result.getMiddleName());
        assertNotNull(result.getLastName());
        assertNotNull(result.getMotherLastName());
        assertNull(result.getContactInformations());
    }

    @Test
    public void mapInWithContactTypeFromContactInformationEmptyTest() throws IOException {

        InputCreateReviewerSubscriptionRequest input = EntityStubs.getInstance()
                .getInputCreateReviewerSubscriptionRequest();
        input.getReviewer().getContactDetails().get(0).setContactType(null);
        ModelReviewerSubscriptionRequest result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getForm().getId());
        assertNotNull(result.getParticipant().getCode());
        assertNotNull(result.getParticipantType().getId());
        assertNotNull(result.getFirstName());
        assertNotNull(result.getMiddleName());
        assertNotNull(result.getLastName());
        assertNotNull(result.getMotherLastName());
        assertNotNull(result.getContactInformations());
        assertNull(result.getContactInformations().get(0).getId());
        assertNotNull(result.getContactInformations().get(0).getContact());
    }


    @Test
    public void mapInWithContactFromContactInformationEmptyTest() throws IOException {

        InputCreateReviewerSubscriptionRequest input = EntityStubs.getInstance()
                .getInputCreateReviewerSubscriptionRequest();
        input.getReviewer().getContactDetails().get(0).setContact(null);
        ModelReviewerSubscriptionRequest result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getForm().getId());
        assertNotNull(result.getParticipant().getCode());
        assertNotNull(result.getParticipantType().getId());
        assertNotNull(result.getFirstName());
        assertNotNull(result.getMiddleName());
        assertNotNull(result.getLastName());
        assertNotNull(result.getMotherLastName());
        assertNotNull(result.getContactInformations());
        assertNotNull(result.getContactInformations().get(0).getId());
        assertNull(result.getContactInformations().get(0).getContact());
    }


    @Test
    public void mapInEmptyTest() {
        ModelReviewerSubscriptionRequest result = mapper
                .mapIn(new InputCreateReviewerSubscriptionRequest());
        assertNotNull(result);
        assertNull(result.getForm());
        assertNull(result.getParticipant());
        assertNull(result.getParticipantType());
        assertNull(result.getFirstName());
        assertNull(result.getMiddleName());
        assertNull(result.getLastName());
        assertNull(result.getMotherLastName());
        assertNull(result.getContactInformations());
    }

    @Test
    public void mapOutFullTest() {
        ModelReviewerSubscriptionResponse response = ResponseFinancialManagementCompaniesMock.getInstance().buildModelReviewerSubscriptionResponse();
        String result = mapper.mapOut(response);

        assertNotNull(result);
        assertEquals(response.getParticipant().getCode(), result);
    }

    @Test
    public void mapOutEmptyTest() {
        ModelReviewerSubscriptionResponse response = ResponseFinancialManagementCompaniesMock.getInstance().buildModelReviewerSubscriptionResponse();
        response.getParticipant().setCode(null);
        String result = mapper.mapOut(response);

        assertNull(result);

        response = ResponseFinancialManagementCompaniesMock.getInstance().buildModelReviewerSubscriptionResponse();
        response.setParticipant(null);
        result = mapper.mapOut(response);

        assertNull(result);

        result = mapper.mapOut(new ModelReviewerSubscriptionResponse());

        assertNull(result);
    }
}
