package com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputGetFMCAuthorizedBusinessManagerProfiledService;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwfp.FormatoKNECFPE0;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwfp.FormatoKNECFPS0;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwfp.mock.FormatsKwfpMock;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.ProfiledService;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.math.BigDecimal;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class TxGetFMCAuthorizedBusinessManagerProfiledServiceMapperTest {

    @InjectMocks
    private TxGetFMCAuthorizedBusinessManagerProfiledServiceMapper mapper;
    @Mock
    private Translator translator;

    @Test
    public void mapInFullTest() {
        InputGetFMCAuthorizedBusinessManagerProfiledService input = EntityStubs.getInstance()
                .getInputGetFinancialManagementCompaniesAuthorizedBusinessManagerProfiledService();
        FormatoKNECFPE0 result = mapper.mapIn(input);

        assertNotNull(result.getCompaid());
        assertNotNull(result.getManagid());
        assertNotNull(result.getCodser());

        assertEquals(input.getFinancialManagementCompanyId(), result.getCompaid());
        assertEquals(input.getAuthorizedBusinessManagerId(), result.getManagid());
        assertEquals(input.getProfiledServiceId(), result.getCodser());
    }

    @Test
    public void mapInEmptyTest() {
        FormatoKNECFPE0 result = mapper.mapIn(new InputGetFMCAuthorizedBusinessManagerProfiledService());

        assertNull(result.getCompaid());
        assertNull(result.getManagid());
        assertNull(result.getCodser());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        FormatoKNECFPS0 format = FormatsKwfpMock.getInstance().getFormatoKNECFPS0();

        when(translator.translateBackendEnumValueStrictly(
                "authorizedBusinessManager.operationsRights.indicators.id", format.getIndser()))
                .thenReturn("QUERY_AND_SIGNATURE");
        when(translator.translateBackendEnumValueStrictly(
                "authorizedBusinessManager.operationsRights.permissionType.id", format.getPermide()))
                .thenReturn("FIXED_DATA");
        when(translator.translateBackendEnumValueStrictly(
                "authorizedBusinessManager.signature.id", format.getPowerid()))
                .thenReturn("JOINT_SIGNATURE_OF_SEVERAL");

        ProfiledService result = mapper.mapOut(format);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getService().getId());
        assertNotNull(result.getService().getName());
        assertNotNull(result.getOperationRights().getIndicators().getId());
        assertNotNull(result.getOperationRights().getIndicators().getName());
        assertNotNull(result.getOperationRights().getPermissionType().getId());
        assertNotNull(result.getOperationRights().getPermissionType().getName());
        assertNotNull(result.getOperationRights().getSignature().getValidationRights().getId());
        assertNotNull(result.getOperationRights().getSignature().getValidationRights().getName());
        assertNotNull(result.getOperationRights().getSignature().getAmountLimit().getAmount());
        assertNotNull(result.getOperationRights().getSignature().getAmountLimit().getCurrency());
        assertNotNull(result.getHasAssignedAuditor());

        assertEquals(format.getIdecon(), result.getId());
        assertEquals(format.getServic(), result.getService().getId());
        assertEquals(format.getServdes(), result.getService().getName());
        assertEquals("QUERY_AND_SIGNATURE", result.getOperationRights().getIndicators().getId());
        assertEquals(format.getDesind(), result.getOperationRights().getIndicators().getName());
        assertEquals("FIXED_DATA", result.getOperationRights().getPermissionType().getId());
        assertEquals(format.getPermdes(), result.getOperationRights().getPermissionType().getName());
        assertEquals("JOINT_SIGNATURE_OF_SEVERAL", result.getOperationRights().getSignature().getValidationRights().getId());
        assertEquals(format.getPowedes(), result.getOperationRights().getSignature().getValidationRights().getName());
        assertEquals(format.getAmount(), result.getOperationRights().getSignature().getAmountLimit().getAmount());
        assertEquals(format.getCurency(), result.getOperationRights().getSignature().getAmountLimit().getCurrency());
        assertTrue(result.getHasAssignedAuditor());
    }

    @Test
    public void mapOutEmptyTest() throws IOException {
        FormatoKNECFPS0 format = FormatsKwfpMock.getInstance().getFormatoKNECFPS0Empty();

        ProfiledService result = mapper.mapOut(format);

        assertNotNull(result);
        assertNull(result.getId());
        assertNull(result.getService());
        assertNull(result.getHasAssignedAuditor());
        assertNotNull(result.getOperationRights().getSignature().getAmountLimit().getAmount());
        assertNull(result.getOperationRights().getSignature().getAmountLimit().getCurrency());
        assertEquals(BigDecimal.ZERO, result.getOperationRights().getSignature().getAmountLimit().getAmount());
    }
}
