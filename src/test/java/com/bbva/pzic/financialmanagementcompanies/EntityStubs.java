package com.bbva.pzic.financialmanagementcompanies;

import com.bbva.pzic.financialmanagementcompanies.business.dto.*;
import com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompanies.ModelFinancialManagementCompaniesData;
import com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompanies.ModelFinancialManagementCompaniesResponse;
import com.bbva.pzic.financialmanagementcompanies.dao.model.ksjo.BodyDataRest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.ksjo.DataOperation;
import com.bbva.pzic.financialmanagementcompanies.dao.model.ksjo.DataOptionals;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.*;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.ObjectMapperHelper;
import com.fasterxml.jackson.core.type.TypeReference;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Entelgy
 */
public final class EntityStubs {

    public static final String SUBSCRIPTIONS_REQUEST_ID = "6";
    public static final String BUSINESS_DOCUMENTS_DOCUMENT_NUMBER = "G";
    public static final String UNIT_MANAGEMENT = "P";
    public static final String BRANCH_ID = "H";
    public static final String FROM_SUBSCRIPTION_REQUEST_DATE = "2018-01-02";
    public static final String TO_SUBSCRIPTION_REQUEST_DATE = "2018-02-28";
    public static final String BUSINESS_ID = "e";
    public static final String PAGINATION_KEY = "q";
    public static final Integer PAGE_SIZE = 3;
    public static final String SUBSCRIPTION_REQUEST_ID = "J";
    public static final String EXPAND = "reviewers,business-managers";
    public static final String EXPAND_ONLY_REVIEWERS = "reviewers";
    public static final String EXPAND_ONLY_BUSINESS_MANAGERS = "business-managers";
    public static final String DNI_DOCUMENT_TYPE_ID_BACKEND = "L";
    public static final String DNI_DOCUMENT_TYPE_ID_ENUM = "DNI";
    public static final String RUC_DOCUMENT_TYPE_ID_BACKEND = "R";
    public static final String RUC_DOCUMENT_TYPE_ID_ENUM = "RUC";
    public static final String FINANCIAL_MANAGEMENT_COMPANY_BUSINESS_MANAGEMENT_TYPE_ID_BACKEND = "I";
    public static final String FINANCIAL_MANAGEMENT_COMPANY_BUSINESS_MANAGEMENT_TYPE_ID_FRONTEND = "JOINT";
    public static final String PRODUCT_ID_BACKEND = "E";
    public static final String PRODUCT_ID_ENUM = "NETCASH_BUSINESS";
    public static final String ACCOUNTS_CAMPAIGNS_OFFERS_PRODUCT_TYPE_BACKEND = "A";
    public static final String ACCOUNTS_CAMPAIGNS_OFFERS_PRODUCT_TYPE_ENUM = "ACCOUNTS";
    public static final String PAYING_ACCOUNT_LOANS_RELATED_CONTRACTS_RELATION_TYPE_BACKEND = "C";
    public static final String PAYING_ACCOUNT_LOANS_RELATED_CONTRACTS_RELATION_TYPE_ENUM = "PAYING_ACCOUNT";
    public static final String JOINT_SIGNATURE_OF_TWO_JOINT_ID_BACKEND = "2";
    public static final String JOINT_SIGNATURE_OF_TWO_JOINT_ID_ENUM = "JOINT_SIGNATURE_OF_TWO";
    public static final String EMAIL_CONTACTS_DETAILS_CONTACT_TYPE_ID_BACKEND = "MA";
    public static final String EMAIL_CONTACTS_DETAILS_CONTACT_TYPE_ID_ENUM = "EMAIL";
    public static final String LEGAL_REPRESENTATIVE_BUSINESS_MANAGERS_ROLE_ID_BACKEND = "LEGAL_REPRESENTATIVE";
    public static final String LEGAL_REPRESENTATIVE_BUSINESS_MANAGERS_ROLE_ID_ENUM = "LEGAL_REPRESENTATIVE";
    public static final String APPROVED_SUSCRIPTION_REQUEST_STATUS_ID_BACKEND = "4";
    public static final String APPROVED_SUSCRIPTION_REQUEST_STATUS_ID_ENUM = "APPROVED";
    public static final String INVALID_DOCUMENT_NUMBER_SUSCRIPTION_REQUEST_STATUS_REASON_ID_BACKEND = "1";
    public static final String INVALID_DOCUMENT_NUMBER_SUSCRIPTION_REQUEST_STATUS_REASON_ID_ENUM = "INVALID_DOCUMENT_NUMBER";
    public static final String EXECUTIVE_SUSCRIPTION_REQUEST_REVIEWER_BACKEND = "E";
    public static final String EXECUTIVE_SUSCRIPTION_REQUEST_REVIEWER_ENUM = "EXECUTIVE";
    public static final String BUSINESS_MANAGER_ID = "B";
    public static final String SUSCRIPTION_REQUEST_REVIEWER_ID_BACKEND = "DM";
    public static final String SUSCRIPTION_REQUEST_REVIEWER_ID_ENUM = "DEPUTY_MANAGER";
    public static final String FINANCIAL_MANAGEMENT_COMPANY_ID = "20072231";
    public final static String AUTHORIZED_BUSINESS_MANAGER_ID = "23kjbd02";
    public final static String PROFILED_SERVICE_ID = "1251";
    public static final String PRODUCT_TYPE_ID_C = "ACCOUNTS";
    public static final String PRODUCT_TYPE_BACKEND_C = "C";
    public static final String PRODUCT_TYPE_ID_T = "CARDS";
    public static final String PRODUCT_TYPE_BACKEND_T = "T";
    public static final String PRODUCT_TYPE_ID_P = "LOANS";
    public static final String PRODUCT_TYPE_BACKEND_P = "P";
    public static final String PRODUCT_TYPE_ID_F = "FUNDS";
    public static final String PRODUCT_TYPE_BACKEND_F = "F";
    public static final String PRODUCT_TYPE_ID_D = "DEPOSITS";
    public static final String PRODUCT_TYPE_BACKEND_D = "D";
    public static final String RELATION_TYPE_ID_C = "PAYING_ACCOUNT";
    public static final String RELATION_TYPE_BACKEND_C = "C";
    public static final String RELATION_TYPE_ID_R = "LINKED_WITH";
    public static final String RELATION_TYPE_BACKEND_R = "R";

    public static final String BUSINESS_MANAGERID = "0026008101000771ADMIN001";
    public static final String WORK_EMAIL = "WORK_EMAIL";
    public static final String ENUM_BACKEND_WORK_EMAIL = "C";
    public static final String AAP = "mock";
    public static final String CONTEXT_PROVIDER_HEADER_APP = "mock";
    public static final String CONTEXT_PROVIDER_HEADER_USER = "e000000";
    public static final String CONTEXT_PROVIDER_HEADER_REQUEST_ID = "8cdbc5c0-9b5c-415d-8861-ea5111a7fac8";
    public static final String CONTEXT_PROVIDER_HEADER_CONTACT_ID = "f6f38b83-7d74";
    public static final String CONTEXT_PROVIDER_HEADER_SERVICE_ID = "SMCPE1810337";

    public static final String CONTEXT_PROVIDER_BODY_COUNTRY = "ES";
    public static final String CONTEXT_PROVIDER_BODY_DATA_OPERATION_BANK = "ou=bbvaespana,o=igrupobbva";
    public static final String CONTEXT_PROVIDER_BODY_DATA_OPERATION_PD_GROUP = "group";
    public static final String CONTEXT_PROVIDER_BODY_DATA_OPTIONALS_LDAP = "ldap1";
    public static final String CONTEXT_PROVIDER_BODY_DATA_OPTIONALS_RAMA = "bbvaespana";
    public static final String CONTEXT_PROVIDER_BODY_DATA_OPTIONALS_GRUPO = "BBVANET_ES";
    public static final String CONTEXT_PROVIDER_BODY_DATA_OPTIONALS_PREFIJO = "0019";

    private static final EntityStubs INSTANCE = new EntityStubs();
    private ObjectMapperHelper objectMapperHelper = ObjectMapperHelper.getInstance();

    private EntityStubs() {
    }

    public static EntityStubs getInstance() {
        return INSTANCE;
    }

    public SubscriptionRequest getSubscriptionRequest() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/subscriptionRequest.json"), SubscriptionRequest.class);
    }

    public DTOIntSubscriptionRequests getDTOIntSubscriptionRequests() {
        DTOIntSubscriptionRequests dtoIntSubscriptionRequests = new DTOIntSubscriptionRequests();
        dtoIntSubscriptionRequests.setData(new ArrayList<>());
        return dtoIntSubscriptionRequests;
    }

    public InputGetSubscriptionRequest getInputGetSubscriptionRequest() {
        InputGetSubscriptionRequest inputGetSubscriptionRequest = new InputGetSubscriptionRequest();
        inputGetSubscriptionRequest.setSubscriptionRequestId(SUBSCRIPTION_REQUEST_ID);
        return inputGetSubscriptionRequest;
    }

    public InputModifySubscriptionRequest getInputModifySubscriptionRequest() throws IOException {
        InputModifySubscriptionRequest inputModifySubscriptionRequest = new InputModifySubscriptionRequest();
        inputModifySubscriptionRequest.setSubscriptionRequest(getSubscriptionRequest());
        inputModifySubscriptionRequest.setSubscriptionRequestId(SUBSCRIPTION_REQUEST_ID);
        return inputModifySubscriptionRequest;
    }

    public InputListSubscriptionRequests getInputListSubscriptionRequests() {
        InputListSubscriptionRequests inputListSubscriptionRequests = new InputListSubscriptionRequests();
        inputListSubscriptionRequests.setSubscriptionsRequestId(EntityStubs.SUBSCRIPTIONS_REQUEST_ID);
        inputListSubscriptionRequests.setBusinessDocumentsBusinessDocumentTypeId(EntityStubs.DNI_DOCUMENT_TYPE_ID_BACKEND);
        inputListSubscriptionRequests.setBusinessDocumentsDocumentNumber(EntityStubs.BUSINESS_DOCUMENTS_DOCUMENT_NUMBER);
        inputListSubscriptionRequests.setStatusId(EntityStubs.APPROVED_SUSCRIPTION_REQUEST_STATUS_ID_BACKEND);
        inputListSubscriptionRequests.setUnitManagement(EntityStubs.UNIT_MANAGEMENT);
        inputListSubscriptionRequests.setBranchId(EntityStubs.BRANCH_ID);
        inputListSubscriptionRequests.setFromSubscriptionRequestDate(EntityStubs.FROM_SUBSCRIPTION_REQUEST_DATE);
        inputListSubscriptionRequests.setToSubscriptionRequestDate(EntityStubs.TO_SUBSCRIPTION_REQUEST_DATE);
        inputListSubscriptionRequests.setBusinessId(EntityStubs.BUSINESS_ID);
        inputListSubscriptionRequests.setPaginationKey(EntityStubs.PAGINATION_KEY);
        inputListSubscriptionRequests.setPageSize(EntityStubs.PAGE_SIZE);
        return inputListSubscriptionRequests;
    }

    public BusinessManagerSubscriptionRequest getBusinessManagerSubscriptionRequest() {
        BusinessManagerSubscriptionRequest businessManagerSubscriptionRequest = new BusinessManagerSubscriptionRequest();
        businessManagerSubscriptionRequest.setTargetUserId("123");
        return businessManagerSubscriptionRequest;
    }

    public InputModifyBusinessManagerSubscriptionRequest getInputModifyBusinessManagerSubscriptionRequest() {
        InputModifyBusinessManagerSubscriptionRequest inputModifySubscriptionRequest = new InputModifyBusinessManagerSubscriptionRequest();
        inputModifySubscriptionRequest.setSubscriptionRequestId(SUBSCRIPTION_REQUEST_ID);
        inputModifySubscriptionRequest.setBusinessManagerId(BUSINESS_MANAGER_ID);
        inputModifySubscriptionRequest.setBusinessManagerSubscriptionRequest(getBusinessManagerSubscriptionRequest());
        return inputModifySubscriptionRequest;
    }

    public InputCreateReviewerSubscriptionRequest getInputCreateReviewerSubscriptionRequest() throws IOException {
        InputCreateReviewerSubscriptionRequest input = new InputCreateReviewerSubscriptionRequest();
        input.setSubscriptionRequestId(SUBSCRIPTION_REQUEST_ID);
        input.setReviewer(getReviewer());
        return input;
    }

    public DTOIntRequestHeader getHeaderRequestBackendContext() {
        DTOIntRequestHeader header = new DTOIntRequestHeader();
        header.setAap(CONTEXT_PROVIDER_HEADER_APP);
        header.setRequestID(CONTEXT_PROVIDER_HEADER_REQUEST_ID);
        header.setContactID(CONTEXT_PROVIDER_HEADER_CONTACT_ID);
        header.setServiceID(CONTEXT_PROVIDER_HEADER_SERVICE_ID);
        header.setUser(CONTEXT_PROVIDER_HEADER_USER);
        return header;
    }

    public DTOIntRequestBody getRequestBody() {
        DTOIntRequestBody body = new DTOIntRequestBody();
        body.setCountry(CONTEXT_PROVIDER_BODY_COUNTRY);
        body.setDataOperationBank(CONTEXT_PROVIDER_BODY_DATA_OPERATION_BANK);
        body.setDataOptionalsLdap(CONTEXT_PROVIDER_BODY_DATA_OPTIONALS_LDAP);
        body.setDataOptionalsRama(CONTEXT_PROVIDER_BODY_DATA_OPTIONALS_RAMA);
        body.setDataOptionalsGrupo(CONTEXT_PROVIDER_BODY_DATA_OPTIONALS_GRUPO);
        body.setDataOptionalsPrefijo(CONTEXT_PROVIDER_BODY_DATA_OPTIONALS_PREFIJO);
        return body;
    }

    public BodyDataRest getBodyDataRest() {
        BodyDataRest bodyDataRest = new BodyDataRest();
        bodyDataRest.setCountry("PERU");
        bodyDataRest.setOperation("create");
        bodyDataRest.setDataOperation(getDataOperation());
        bodyDataRest.setDataOptionals(getDataOptionals());
        return bodyDataRest;
    }

    private DataOperation getDataOperation() {
        DataOperation dataOperation = new DataOperation();
        dataOperation.setAlias("123");
        dataOperation.setBank("1234");
        return dataOperation;
    }

    private DataOptionals getDataOptionals() {
        DataOptionals dataOptionals = new DataOptionals();
        dataOptionals.setLdap("1");
        dataOptionals.setRama("2");
        dataOptionals.setGrupo("3");
        dataOptionals.setPrefijo("4");
        return dataOptionals;
    }

    public InputSendEmailOtpFinancialManagementCompaniesBusinessManager getInputSendMailRequest() {
        InputSendEmailOtpFinancialManagementCompaniesBusinessManager input = new InputSendEmailOtpFinancialManagementCompaniesBusinessManager();
        input.setBusinessManagerId(BUSINESS_MANAGERID);
        DTOIntEmail dtoIntEmail = new DTOIntEmail();
        DTOIntType dtoIntType = new DTOIntType();
        dtoIntType.setId(WORK_EMAIL);
        dtoIntEmail.setType(dtoIntType);
        dtoIntEmail.setValue("nobody@fakemail.com");
        input.setEmailList(new ArrayList<>());
        input.getEmailList().add(dtoIntEmail);
        input.setUserName("Carlos P");
        input.setBusinessName("Jose C");
        input.setPassword("clave1234");
        return input;
    }

    public DTOIntRequestBody getRequestBodyWithPdGroup() {
        DTOIntRequestBody body = getRequestBody();
        body.setDataOperationPdgroup(CONTEXT_PROVIDER_BODY_DATA_OPERATION_PD_GROUP);
        return body;
    }

    public InputCreateFinancialManagementCompaniesRelatedContracts getInputCreateFinancialManagementCompaniesRelatedContracts() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/inputCreateFinancialManagementCompaniesRelatedContracts.json"),
                InputCreateFinancialManagementCompaniesRelatedContracts.class);
    }

    public List<RelatedContract> getRelatedContracts() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/relatedContracts.json"), new TypeReference<List<RelatedContract>>() {
        });
    }

    public Reviewer getReviewer() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/reviewer.json"), Reviewer.class);
    }

    public DTOIntBusinessManager getDTOIntBusinessManager() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/dtoIntBusinessManager.json"), DTOIntBusinessManager.class);
    }

    public InputCreateAuthorizedBusinessManager getInputCreateFinancialManagementCompaniesAuthorizedBusinessManager() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/inputCreateAuthorizedBusinessManager.json"), InputCreateAuthorizedBusinessManager.class);
    }

    public BusinessManager getBusinessManager() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/businessManager.json"), BusinessManager.class);
    }

    public FinancialManagementCompanies getFinancialManagementCompanies() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/financialManagementCompanies.json"), FinancialManagementCompanies.class);
    }

    public CreateAuthorizedBusinessManager getCreateAuthorizedBusinessManager() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/createAuthorizedBusinessManager.json"), CreateAuthorizedBusinessManager.class);
    }

    public InputRequestBackendRest getInputDelUser() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("mock/inputPasswordCreations.json"), InputRequestBackendRest.class);
    }

    public InputSendEmailOtpFinancialManagementCompaniesBusinessManager getInputSendEmailOtp() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("mock/InputSendMailOtp.json"), InputSendEmailOtpFinancialManagementCompaniesBusinessManager.class);
    }

    public InputGetFMCAuthorizedBusinessManagerProfiledService getInputGetFinancialManagementCompaniesAuthorizedBusinessManagerProfiledService() {
        InputGetFMCAuthorizedBusinessManagerProfiledService input = new InputGetFMCAuthorizedBusinessManagerProfiledService();
        input.setFinancialManagementCompanyId(FINANCIAL_MANAGEMENT_COMPANY_ID);
        input.setAuthorizedBusinessManagerId(AUTHORIZED_BUSINESS_MANAGER_ID);
        input.setProfiledServiceId(PROFILED_SERVICE_ID);
        return input;
    }

    public ValidateOperationFeasibility getValidateOperationFeasibility() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("mock/validateOperationFeasibility.json"), ValidateOperationFeasibility.class);
    }

    public InputValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility getInputValidateFinancialManagementCompaniesAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/validateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility.json"), InputValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility.class);
    }

    public InputListFMCAuthorizedBusinessManagersProfiledServicesContracts getInputListFinancialManagementCompaniesAuthorizedBusinessManagersProfiledServicesContracts() {
        InputListFMCAuthorizedBusinessManagersProfiledServicesContracts input = new InputListFMCAuthorizedBusinessManagersProfiledServicesContracts();
        input.setFinancialManagementCompanyId(FINANCIAL_MANAGEMENT_COMPANY_ID);
        input.setAuthorizedBusinessManagerId(AUTHORIZED_BUSINESS_MANAGER_ID);
        input.setProfiledServiceId(PROFILED_SERVICE_ID);
        input.setPaginationKey(PAGINATION_KEY);
        input.setPageSize(PAGE_SIZE);
        return input;
    }

    public DTOIntFinancialManagementCompanies getDTOIntFinancialManagementCompanies() throws IOException{
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/financialManagementCompanies.json"), DTOIntFinancialManagementCompanies.class);
    }

    public ModelFinancialManagementCompaniesResponse getModelFinancialManagementCompaniesResponse() throws IOException{
        ModelFinancialManagementCompaniesResponse response = new ModelFinancialManagementCompaniesResponse();
        ModelFinancialManagementCompaniesData data = new ModelFinancialManagementCompaniesData();
        data.setId(FINANCIAL_MANAGEMENT_COMPANY_ID);
        response.setData(data);
        return response;
    }

}
